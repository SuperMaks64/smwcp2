;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; "Vertical Auto Scroll" generator
; By Jimmy52905
; WARNING: Do NOT place this generator in the same screen as the MAIN entrance. Place it
; at least one screen higher instead, or else a strange glitch will occur.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
!FLAG = $13D8
	PRINT "INIT ",pc
	STZ !FLAG
	RTL
	PRINT "MAIN ",pc                                    
	PHB                     
	PHK                     
	PLB                     
	JSR MAIN 
	PLB                     
	RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MAIN:
	LDA $1463
	CMP #$0E	;If at screen 0E or passed...
	BCS RETURN

	LDA $71		;\
	CMP #$09	; |
	BEQ RETURN	; |If Mario is dying...
			; |
	CMP #$06	; |
	BEQ RETURN	; |
			; |
	CMP #$05	; |
	BEQ RETURN	; |Or changing to another level...
			; |
	CMP #$01	; |Or getting hurt...
	BEQ RETURN	;/ Go to "RETURN".

    LDA $9D
	BNE SPRITESLOCKED ; branch to a place where the code isnt run


	LDA $13		;\
	LSR A		; |
	BCC RETURN	;/ Only run code every other frame.

	STZ $1412	;Disable Vertical Scroll so that the code can run properly.

	
	
	
	
	
	
	
	
	
	
	
	
	LDA !FLAG		;check up/down flag
	BNE GODOWN		;If the flag is not 0 (going up), we will go down instead.

	LDA $1464	;\ Before going up, we must first check...
        BEQ GODOWNFLAG	;/If it is at the top already, we must go down. However, if it is not at the top yet....

	REP #$20	;16-bit (accumulator).
	DEC $1464	;Make the screen scroll upwards automatically.
	SEP #$20	;8-bit (accumulator).

LDA $14
AND #$3F	;01, 03, 07, 0F, 1F, 3F, 7F, FF (faster to slower)
BNE NOSFX
LDA #$60
STA $1DFC
NOSFX:

	RTS

GODOWN:
	LDA $1464	;\Before going down, we must first check...
	CMP #$C0	; |If the screen position is already at the bottom of the level or not.
	BEQ GOUPFLAG	;/If it is at the bottom already, we must go up. However, if it is not at the bottom yet....

	REP #$20	;16-bit (accumulator).
	INC $1464	;Make the screen scroll downwards automatically.
	SEP #$20	;8-bit (accumulator).
	
	LDA $14
AND #$3F	;01, 03, 07, 0F, 1F, 3F, 7F, FF (faster to slower)
BNE NOSFX
LDA #$61
STA $1DFC
NOSFX_TWO::
	RTS

GODOWNFLAG:
	LDA #$01
	STA !FLAG	;Going down flag.
	RTS		;RETURN from Subroutine.
GOUPFLAG:
	STZ !FLAG	;Going up flag.

SPRITESLOCKED:
RETURN:
	RTS