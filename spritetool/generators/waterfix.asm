;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Layer 3 low tide Vscroll fix / custom height
;
; Programmed by SMWEdit.
;
; Note: This does not work in vertical levels.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		    !HEIGHT_DIFF = $0080	; $C0 - layer 3 Yposition ( ? + H = $C0 )
					; default low tide Yposition is 40 ( ? = $0080 )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		    PRINT "INIT ",pc
		    PRINT "MAIN ",pc
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SPRITE_CODE_START:   PHP
		    REP #%00110100
		    LDA $1C		; \
		    CMP.w #!HEIGHT_DIFF	;  |  compare screen Y position
		    BCS VARH		;  |  with $C0 - desired Y
		    BCC ZERO		; /

VARH:		    LDA $1C		; \
		    SBC.w #!HEIGHT_DIFF	;  |  set layer 3 Y
		    STA $24		;  |  to $C0 - desired height
		    PLP			;  |  if screen Y > or = the value
		    RTS			; /

ZERO:		    LDA.w #$0000	; \
		    STA $24		;  |  set layer 3 Y to ZERO
		    PLP			;  |  if screen Y < the value
		    RTS			; /