;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cave-In Generator, by yoshicookiezeus
;;
;; Description: Spawns Cave-In Blocks at random x positions at a set height.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!BlockNumber		= $AD				; sprite number the block sprite is inserted as

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


			print "INIT ",pc
			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR SpriteMain
			PLB
			RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteMain:
			LDA $14	 			;\ if not yet time to spawn new block,
			AND #$1F			; |
			BNE Return			;/ return

			LDA #$40			;\ shake ground
			STA $1887			;/

			JSL $02A9DE			;\ if no empty slots,
			BMI Return			;/ return

			TYX

			LDA #$01			;\ set sprite state for new sprite
			STA $14C8,x			;/

			LDA #$A0			;\ set new sprite y position - THIS LINE CONTROLS THE HEIGHT WHERE THE BLOCKS ARE SPAWNED
			STA $D8,x			; |
			STZ $14D4,x			;/

			JSL $01ACF9			; random number generation subroutine
			REP #$20			;\ set new sprite y position
			LDA $148B			; |
			AND #$00FF			; |
			CLC				; |
			ADC $1A				; |
			SEP #$20			; |
			STA $E4,x			; |
			XBA				; |
			STA $14E0,x			;/

			LDA #!BlockNumber		;\ set new sprite number
			STA $7FAB9E,x			;/
			JSL $07F7D2			; clear out sprite tables
			JSL $0187A7			; get table values for custom sprite
			LDA $148B			;\ set extra bit randomly
			AND #$04			;/
			ORA #$88			;\ mark sprite as custom
			STA $7FAB10,x			;/
Return:
			RTS

