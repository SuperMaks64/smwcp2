;=======================================
;Pirhangler
; By Atma
;=======================================
incsrc subroutinedefs_xkas.asm
print "INIT ",pc
PHB		;\
PHK		; | Change the data bank to the one our code is running from.
PLB		;/ This is a good practice.
JSR SubHorzPos	;\
TYA		; | Use if enemy doesn't change direction in main code.
STA $157C,x	;/
PLB		;\ Restore old data bank.
RTL		;/ And return.

print "MAIN ",pc
PHB		;\
PHK		; | Change the data bank to the one our code is running from.
PLB		; | This is a good practice.
JSR SpriteCode	; | Jump to the sprite's function.
PLB		; | Restore old data bank.
RTL		;/ And return.

;===================================
;Defines
;===================================
Xspd: db $10,$F0
Xspd2: db $1C,$E4
;===================================
;Routines
;===================================

Attack:
LDY #$07
ExtraLoop:
LDA $170B,y
BEQ Extra1
DEY
BPL ExtraLoop
RTS

Extra1:
LDA #$0D
STA $170B,y
LDA $E4,x
STA $171F,y ; X position low byte
LDA $14E0,x
STA $1733,y ; X position high byte
LDA $D8,x
STA $1715,y ; Y position low byte
LDA $14D4,x
STA $1729,y ; Y position high byte
LDA #$00
STA $173D,y
PHY
LDY $157C,x
LDA Xspd2,y
PLY
STA $1747,y
LDA #$FF
STA $176F,y
RTS

;===================================
;Sprite Function
;===================================
RETURN: RTS
SpriteCode:
	JSR Graphics
	LDA $14C8,x			;\
	CMP #$08			; | If sprite dead,
	BNE RETURN			;/ Return.
	LDA $9D				;\
	BNE RETURN			;/ If locked, return.
	JSR SubOffscreenX0		; Handle offscreen
	JSL $01A7DC
	JSL $019138
	LDA $164A,x
	BNE Underwater
	LDA $1588,x
	AND #$04
	BNE Jump
	LDA $151C,x
	BNE AttackTimer
	INC $151C,x
	STZ $C2,x
	LDA $14
	AND #$07
	CLC
	ADC #$10
	STA $1510,x
Jump:
	LDA #$B0
	STA $AA,x
AttackTimer:
	LDY $157C,x
	LDA Xspd,y
	STA $B6,x
	JSL $01802A
	INC $1504,x
	LDA $1504,x
	CMP $1510,x
	BCC RETURN
	JSR Attack
	STZ $1504,x
	RTS
Underwater:
	LDA $151C,x
	BEQ Swimming
	STZ $151C,x
	STZ $1504,x
	STZ $AA,x
	JSL $01801A
Swimming:
	LDA $C2,x
	BNE SwimDown
	INC $AA,x
	LDA $AA,x
	CMP #$14
	BNE SwimJSL
	INC $C2,x
	BRA SwimJSL
SwimDown:
	DEC $AA,x
SwimJSL:
	LDY $157C,x
	LDA Xspd,y
	STA $B6,x
	JSL $01801A
	JSL $018022
	RTS
;===================================
;Graphics Code
;===================================
TileMap:	db $80,$82
		db $84,$86
		db $80,$8A
		db $88,$86
Xoffset:	db $F0,$00,$00,$F0
Properties:	db $09,$0B,$09,$0B

Graphics:
        JSL !GetDrawInfo         ; sets y = OAM offset
	LDA $157C,x             ; \ $02 = direction
	EOR #$01
	ASL A
	STA $02                 ; /     

	LDA $14
	LSR
	LSR
	LSR
	AND #$03
	ASL A
	STA $03             

	PHX
	LDX #$01
Loop:
	PHX
	TXA
	CLC
	ADC $02
	TAX
	LDA Xoffset,x
	CLC
	ADC $00                 ; \ tile x position = sprite y location ($01)
	STA $0300,y             ; /
	PLX

	LDA $01                 ; \ tile y position = sprite x location ($00)
	STA $0301,y             ; /

	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA TileMap,x
	STA $0302,y 
	PLX
	
	PHX
	TXA
	CLC
	ADC $02
	TAX
	LDA Properties,x
	LDX $02
	BEQ NoFlip              ;  |
	ORA #$40                ; /    ...flip tile
NoFlip:
	;ORA $64                 ; add in tile priority of level
	AND #$CF
	STA $0303,y             ; store tile properties
	PLX
	INY	 		; \ increase index to sprite tile map ($300)...
	INY			;  |    ...we wrote 1 16x16 tile...
	INY	 		;  |    ...sprite OAM is 8x8...
	INY	 		; /    ...so increment 4 times
	DEX
	BPL Loop

	PLX

	LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
	LDA #$01                ;  | A = (number of tiles drawn - 1)
	JSL $01B7B3             ; / don't draw if offscreen
	RTS	 		; return

      
Table3:              db $40,$B0
Table6:              db $01,$FF
Table4:              db $30,$C0,$A0,$C0,$A0,$F0,$60,$90,$30,$C0,$A0,$80,$A0,$40,$60,$B0
Table5:              db $01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$00,$01,$FF

SubOffscreenX0:
LDA #$00
STA $03

JSR SubIsOffscreen
BEQ Return2

LDA $5B
LSR
BCS VerticalLevel
LDA $D8,x
CLC
ADC #$50
LDA $14D4,x
ADC #$00
CMP #$02
BPL EraseSprite
LDA $167A,x
AND #$04
BNE Return2
LDA $13
AND #$01
ORA $03
STA $01
TAY
LDA $1A
CLC
ADC Table4,y
ROL $00
CMP $E4,x
PHP
LDA $1B
LSR $00
ADC Table5,y
PLP
SBC $14E0,x
STA $00
LSR $01
BCC Label20
EOR #$80
STA $00
Label20:
LDA $00
BPL Return2

EraseSprite:
LDA $14C8,x
CMP #$08
BCC KillSprite
LDY $161A,x
CPY #$FF
BEQ KillSprite
LDA #$00
STA $1938,y
KillSprite:
STZ $14C8,x
Return2:
RTS

VerticalLevel:

LDA $167A,x
AND #$04
BNE Return2
LDA $13
LSR
BCS Return2
AND #$01
STA $01
TAY
LDA $1C
CLC
ADC Table3,y
ROL $00
CMP $D8,x
PHP
LDA $1D
LSR $00
ADC Table6,y
PLP
SBC $14D4,x
STA $00
LDY $02
BEQ Label22
EOR #$80
STA $00
Label22:
LDA $00
BPL Return2
BMI EraseSprite

SubIsOffscreen:
LDA $15A0,x
ORA $186C,x
RTS

SubHorzPos:
LDY #$00
LDA $94
SEC
SBC $E4,x
STA $0F
LDA $95
SBC $14E0,x
BPL $01
INY
RTS