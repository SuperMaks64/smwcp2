;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

incsrc subroutinedefs_xkas.asm

!FrameNum = $C2
!Direction = $157C

                    print "INIT ",pc
					STZ !FrameNum,x
					LDA #$00
					STA !Direction,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $E0,$20
SendReturn:
JMP return

SPRITE_ROUTINE:	  	
					LDA $9D
					BNE SendReturn
					LDA $14C8,x
					CMP #$08
					BNE SendReturn
					JSL $01802A
					PHX
					LDA !Direction,x
					TAX
					LDA XSpeeds,x
					PLX
					STA $B6,x
					STZ !FrameNum,x
					LDA $15B8,x
					BEQ .NoSlope
					BPL .RightSlope
					LDA !Direction,x
					BNE +
					LDA #$04
					STA !FrameNum,x
					BRA .NoSlope
					+
					LDA #$02
					STA !FrameNum,x
					BRA .NoSlope
					.RightSlope
					LDA !Direction,x
					BNE +
					LDA #$02
					STA !FrameNum,x
					BRA .NoSlope
					+
					LDA #$04
					STA !FrameNum,x
					.NoSlope
					
					return:
					JSR SUB_OFF_SCREEN_X0
					LDY $15EA,x             ; get offset to sprite OAM
					STY $04
					JSR SUB_GFX					;Draw the graphics					
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00
					LDA $94
					SEC
					SBC $E4,x
					STA $0F
					LDA $95
					SBC $14E0,x
					BPL +
					INY
					+
					RTS

TileSize:
db $02,$02,$02,$02,$00,$00
					
TilePointers:
dw Straight
dw Up1
dw Up2
dw Down1
dw Down2

XOffPointers:
dw StraightX
dw Up1X
dw Up2X
dw Down1X
dw Down2X

YOffPointers:
dw StraightY
dw Up1Y
dw Up2Y
dw Down1Y
dw Down2Y

Straight:
db $90,$92,$A0,$A2,$80,$80
Up1:
db $94,$96,$A4,$A6,$80,$80
Up2:
db $98,$9A,$A8,$AA,$80,$80
Down1:
db $C4,$C6,$E4,$E6,$80,$80
Down2:
db $C8,$CA,$E8,$EA,$80,$80

StraightX:
db $F8,$08,$F8,$08,$FC,$08
Up1X:
db $F8,$08,$F8,$08,$00,$0C
Up2X:
db $F8,$08,$F8,$08,$06,$0E
Down1X:
db $F8,$08,$F8,$08,$FC,$08
Down2X:
db $F8,$08,$F8,$08,$FA,$02

StraightY:
db $F4,$F4,$FC,$FC,$08,$08
Up1Y:
db $F4,$F4,$FC,$FC,$08,$00
Up2Y:
db $F4,$F4,$FC,$FC,$06,$FE
Down1Y:
db $F4,$F4,$04,$04,$00,$08
Down2Y:
db $F0,$F0,$00,$00,$FC,$04

SUB_GFX:            
					LDA !Direction,x
					STA $0C
					JSL !GetDrawInfo       	;Get all the drawing info, duh
					LDY $04
					PHX
					LDA $14
					LSR #2
					AND #$03
					ASL #6
					STA $0B
					LDA !FrameNum,x
					ASL
					TAX
					REP #$20
					LDA TilePointers,x
					STA $05
					LDA XOffPointers,x
					STA $07
					LDA YOffPointers,x
					STA $09
					SEP #$20
					LDX #$05
					.GFXLoop
					
					PHY
					TXY
					LDA ($07),y
					STA $0D
					LDA $00
					LDY $0C
					BEQ +
					CLC
					ADC $0D
					BRA ++
					+
					SEC
					SBC $0D
					CPX #$04
					BCC ++
					CLC
					ADC #$08
					++
					PLY
					STA $0300,y
					
					PHY
					TXY
					LDA ($09),y
					PLY
					CLC
					ADC $01
					STA $0301,y
					
					PHY
					TXY
					LDA ($05),y
					PLY
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					CPX #$04
					BCC +
					ORA $0B
					+
					PHX
					LDX $0C
					BNE +
					ORA #$40
					+
					PLX
					ORA $64
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA #$05
					LDY #$FF
					JSL $01B7B3					;/and then draw em
					EndIt:
					RTS
					
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:  LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13
                    AND #$01
                    ORA $03
                    STA $01
                    TAY
                    LDA $1A
                    CLC
                    ADC SPR_T14,y
                    ROL $00
                    CMP $E4,x
                    PHP
                    LDA $1B
                    LSR $00
                    ADC SPR_T15,y
                    PLP
                    SBC $14E0,x
                    STA $00
                    LSR $01
                    BCC SPR_L31
                    EOR #$80
                    STA $00
SPR_L31:             LDA $00
                    BPL RETURN_35
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x
                    CPY #$FF
                    BEQ KILL_SPRITE
                    LDA #$00
                    STA $1938,y
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13
                    LSR A
                    AND #$01
                    STA $01
                    TAY
                    LDA $1C
                    CLC
                    ADC SPR_T12,y
                    ROL $00
                    CMP $D8,x
                    PHP
                    LDA.w $001D 
                    LSR $00
                    ADC SPR_T13,y
                    PLP
                    SBC $14D4,x
                    STA $00
                    LDY $01
                    BEQ SPR_L38
                    EOR #$80
                    STA $00
SPR_L38:             
					LDA $00
                    BPL RETURN_35
                    BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return