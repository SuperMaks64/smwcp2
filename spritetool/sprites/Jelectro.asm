;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; World 4A Boss (Jelectro)
;;
;;    Created by cstutor89
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

incsrc subroutinedefs_xkas.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!HIT_POINTS = $03
	!DANGER_ZONE = $02
	!ORIGINAL_PALETTE = $3D
	!ELEC_PALETTE = $35
	!HURT_PALETTE = $39

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INIT/MAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc
	LDA #$00
	STA $C2,x
	LDA #$FF
	STA $1540,x
	LDA #$00
	STA $7F888C
	LDA #!ORIGINAL_PALETTE 
	STA $15F6,x
	LDA #$08
	STA $14C8,x
	RTL

PRINT "MAIN ",pc
	PHB		;\
	PHK		; | Change the data bank to the one our code is running from.
	PLB		; | This is a good practice.
	JSR SPRITECODE	; | Jump to the sprite's function.
	PLB		; | Restore old data bank.
	RTL		;/ And RETURN.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite Main Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITECODE:	JSR GRAPHICS
		LDA $9D
		BNE RETURN
		JSR HITDETECTION
		JSL $01A7DC
		BCC CHOOSEST
		JSL $00F5B7
CHOOSEST:	LDA $C2,x
		BEQ STATE_0
		CMP #$01
		BEQ STATE_1
		CMP #$02
		BEQ STATE_2
		CMP #$03
		BEQ STATE_3
		CMP #$04
		BEQ STATE_4
		CMP #$05
		BEQ STATE_5
		CMP #$06
		BEQ STATE_6
		CMP #$07
		BEQ STATE_7
		CMP #$08
		BEQ STATE_8
		CMP #$09
		BEQ STATE_9
		CMP #$0A
		BEQ STATE_A
		BRA RETURN
STATE_0:	JMP FOLLOW
STATE_1:	JMP STALL
STATE_2:	JMP FLYUP
STATE_3:	JMP ELECTRICITY
STATE_4:	JMP HOMING
STATE_5:	JMP SWEEP
STATE_6:	JMP HURT
STATE_7:	JMP GOAL
STATE_8:	JMP POSITIONUP
STATE_9:	JMP POSITIONDOWN
STATE_A:	JMP RAIN
RETURN:		RTS

HITDETECTION:	LDA $1558,x
		BNE RETURN	
		LDA $14D4,x
		BNE RETURN
		LDA $D8,x
		CMP #$88
		BCC RETURN
		CMP #$D9
		BCS RETURN
		LDA $14E0,x
		BEQ X1_CHECK
		CMP #$FF
		BEQ X2_CHECK
		BRA RETURN

X1_CHECK:	LDA $E4,x
		CLC
		ADC #$07
		cmp #$E0
		bcs +
		CMP #$D0
		BCS WAS_HIT
		LDA $E4,x
		cmp #$09
		bcc +
		CMP #$19
		BCC WAS_HIT	
		+
		RTS

X2_CHECK:	LDA $E4,x
		CMP #$F8
		BCS WAS_HIT
		RTS

WAS_HIT:	INC $1528,x		; Increase the HP's used.
		LDA #$68
		STA $1540,x
		STA $1558,x		; Set HURT Invincibility
		LDA #$06
		STA $C2,x		; Set Jelectro's State to HURT (6).
		LDA #$28                ; \ sound effect
		STA $1DFC               ; /
		STZ $AA,x
		STZ $B6,x
		LDA #!HURT_PALETTE
		STA $15F6,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 0: FOLLOW Mario
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ACCELERATIONX:	db $01,$FF
MAXSPEEDX:	db $0C,$F4
ACCELERATIONY:	db $01,$FF
MAXSPEEDY:	db $0C,$F4
STATE_MACH:	db $00,$01,$02,$00,$00,$01,$04,$00,$01,$03,$00,$01,$08,$00,$01,$04
STATE_TIME:	db $FF,$40,$58,$FF,$FF,$FF,$FF,$FF,$FF

FOLLOW:		LDA $1540,x
		BEQ NEXT_STATE
		LDA $14
		AND #$01
		BNE APPLYSPEED
		JSR SUBHORZPOS		;\ if max horizontal speed in the appropriate
		LDA $B6,x		; | direction achieved,
		CMP MAXSPEEDX,y		; |
		BEQ MAXXSPEED		;/ don't change horizontal speed
		CLC			;\ else,
		ADC ACCELERATIONX,y	; | accelerate in appropriate direction
		STA $B6,x		;/
MAXXSPEED:	JSR SUBVERTPOS		;\ if max vertical speed in the appropriate
		LDA $AA,x		; | direction achieved,
		CMP MAXSPEEDY,y		; |
		BEQ APPLYSPEED		;/ don't change vertical speed
		CLC			;\ else,
		ADC ACCELERATIONY,y	; | accelerate in appropriate direction
		STA $AA,x		;/
APPLYSPEED:	JSL $018022		; apply x speed
		JSL $01801A		; apply y speed
		RTS

NEXT_STATE:	PHX
		LDA $7F888C
		CLC
		ADC #$01
		AND #$0F
		STA $7F888C
		TAX
		LDA STATE_MACH,x
		PLX
		STA $C2,x
		PHX
		TAX
		LDA STATE_TIME,x
		PLX
		STA $1540,x
		STZ $AA,x
		STZ $B6,x
		LDA $14D4,x
		STA $192C
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 1: STALL Time
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STALL:		LDA $1540,x
		BEQ NEXT_STATE
		AND #$1F
		TAY
		LDA SWEEP_SPEEDX,y
		STA $AA,x		
		JSL $01801A		
		RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 2: Fly To Corner
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FLY_DIRECT:	db $08,$F8

FLYUP:		LDA $D8,x
		CMP #$30
		BCC CHANGE_FLY
		LDA $1540,x
		CMP #$57
		BNE CALC_FLY
		JSR DETER_DIR
CALC_FLY:	DEC $AA,x
		JSL $018022		; apply x speed
		JSL $01801A		; apply y speed
		RTS

DETER_DIR:	JSL $01ACF9
		LDA $148D
		AND #$01
		TAY
		LDA FLY_DIRECT,y
		STA $B6,x
		STZ $1936
		LDA #$01
		STA $1937
		LDA $B6,x
		CMP #$10
		BNE END_FLY
		LDA #$02
		STA $1936
		LDA #$FF
		STA $1937
END_FLY:	RTS

CHANGE_FLY:	LDA #$05
		STA $C2,x
		LDA #$02
		STA $1540,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 3: Shoot ELECTRICITY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

T_SPEEDX:	db $24,$DC,$40,$C0
T_SPEEDY:	db $00,$00,$00,$00

ELECTRICITY:	LDA $1540,x
		BNE SHOOTCHARGE
		JMP NEXT_STATE
SHOOTCHARGE:	LDA $1540,x
		AND #$3F
		BEQ SHOOTELEC
		CMP #$28
		BCS GLOWELEC
		LDA #!ORIGINAL_PALETTE  
		STA $15F6,x
		JMP FOLLOW
		
SHOOTELEC:	LDA #!ORIGINAL_PALETTE 
		STA $15F6,x
		LDY #$01
		LDA $1528,x
		CMP #!DANGER_ZONE
		BCC NO_8SHOT
		LDY #$03
NO_8SHOT:	LDA #$06
		STA $170B,y
		LDA $E4,x
		CLC
		ADC #$08
		STA $171F,y
		LDA $14E0,x
		STA $1733,y
		LDA $D8,x
		CLC
		ADC #$08
		STA $1715,y
		LDA $14D4,x
		STA $1729,y
		LDA T_SPEEDY,y
		STA $173D,y
		LDA T_SPEEDX,y
		STA $1747,y
		LDA #$FF
		STA $176F,y
		DEY
		BPL NO_8SHOT
END_SHOTS:	LDA #$10
		STA $1DF9
		RTS

GLOWELEC:	LDA #!ELEC_PALETTE 
		STA $15F6,x
		RTS	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 4: HOMING 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HOMING_ACCX:	db $02,$FE
HOMING_MAXX:	db $30,$D0
HOMING_ACCY:	db $02,$FE
HOMING_MAXY:	db $30,$D0

HOMING:		LDA $1540,x
		BNE MOVE_HOME
		JMP NEXT_STATE
MOVE_HOME:	LDA $1540,x
		AND #$7F
		CMP #$60
		BEQ MAKE_NOISE
		BCC MOVE_JEL
		BRA HOM_STALL
MAKE_NOISE:	LDA #$25
		STA $1DF9
HOM_STALL:	LDA $1540,x
		AND #$1F
		TAY
		LDA SWEEP_SPEEDX,y
		STA $AA,x		
		JSL $01801A		
		RTS

MOVE_JEL:	LDA $13
		AND #$01
		BNE APPLYSPEED2
		JSR SUBHORZPOS		;\ if max horizontal speed in the appropriate
		LDA $B6,x		; | direction achieved,
		CMP HOMING_MAXX,y		; |
		BEQ HOMXSPEED		;/ don't change horizontal speed
		CLC			;\ else,
		ADC HOMING_ACCX,y	; | accelerate in appropriate direction
		STA $B6,x		;/
HOMXSPEED:	JSR SUBVERTPOS		;\ if max vertical speed in the appropriate
		LDA $AA,x		; | direction achieved,
		CMP HOMING_MAXY,y		; |
		BEQ APPLYSPEED2		;/ don't change vertical speed
		CLC			;\ else,
		ADC HOMING_ACCY,y	; | accelerate in appropriate direction
		STA $AA,x		;/
APPLYSPEED2:	JSL $018022		; apply x speed
		JSL $01801A		; apply y speed
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 5: SWEEP Grounds
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

START_POSX:	db $48,$70,$A8
START_POSY_LO:	db $28,$48,$28
START_POSY_HI:	db $00,$01,$00
FINISH_POSY_LO:	db $48,$28,$48
FINISH_POSY_HI:	db $01,$00,$01
SWEEP_SPEEDX:	db $00,$01,$02,$04,$07,$0B,$0F,$16,$16,$0F,$0B,$07,$04,$02,$01,$00,$FF,$FE,$FD,$FB,$F9,$F4,$F0,$EA,$EA,$F0,$F4,$F9,$FB,$FD,$FE,$FF
SWEEP_SPEEDY:	db $1C,$D4,$1C

SWEEP:		LDA $1540,x
		CMP #$01
		BEQ FIRSTSWEEP
		LDY $1936
		LDA FINISH_POSY_HI,y
		CMP $14D4,x
		BNE NOTTHEREYET
		LDA $1936
		CMP #$01
		BEQ GOING_UP
		LDA FINISH_POSY_LO,y
		CMP $D8,x
		BCC CHANGESWEEP
		BRA NOTTHEREYET
GOING_UP:	LDA FINISH_POSY_LO,y
		CMP $D8,x
		BCS CHANGESWEEP
NOTTHEREYET:	LDA SWEEP_SPEEDY,y
		STA $AA,x
		LDA $1564,x
		AND #$3F
		SEC
		SBC #$20
		STA $B6,x
		LDA $1564,x
		AND #$40
		BEQ UPDATE_SWEEP
		LDA $B6,x
		EOR #$FF
		STA $B6,x
UPDATE_SWEEP:	JSL $018022		; apply x speed
		JSL $01801A		; apply y speed
		RTS
STOPSWEEP:	JMP NEXT_STATE

CHANGESWEEP:	LDA #$C0
		STA $1564,x
		LDA $1936
		CMP #$01
		BEQ NEXT_SWEEP
NEXT_SWEEP:	LDA $1936
		CLC
		ADC $1937
		STA $1936
		CMP #$FF
		BEQ STOPSWEEP
		CMP #$03
		BEQ STOPSWEEP
FIRSTSWEEP:	LDA #$C0
		STA $1564,x
		LDY $1936
		LDA START_POSX,y
		STA $E4,x
		STZ $14E0,x
		LDA START_POSY_LO,y
		STA $D8,x
		LDA START_POSY_HI,y
		STA $14D4,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 6: HURT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HURT:		LDA $1558,x
		BNE CHECKDEATH
		LDA $1528,x
		CMP #!DANGER_ZONE
		BNE GOSTATE
		LDA #$01
		STA $14AF
GOSTATE:	LDA #!ORIGINAL_PALETTE
		STA $15F6,x
		JMP NEXT_STATE
CHECKDEATH:	LDA $1528,x
		CMP #!HIT_POINTS
		BEQ SETDSTATE
		JMP FOLLOW
		
SETDSTATE:	LDA #$20
		STA $1540,x
		LDA #$07
		STA $C2,x
		RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 7: GOAL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
GOAL:		LDA $1540,x
		BNE GOAL_WAIT
		LDA #$04
		STA $14C8,x             ; Destroy Sprite 
		LDA #$1F
		STA $1540,x
		LDA #$08
		STA $1DF9
		INC $13C6               ; Prevent Mario from walking.
		LDA #$FF                ; \ Set GOAL
		STA $1493               ; /
		LDA #$03                ; \ Set Ending Eusic
		STA $1DFB               ; /
GOAL_WAIT:	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 8: Going Up Position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
POSITIONUP:	LDA $14D4,x
		BNE G_UP
		LDA $D8,x
		CMP #$30
		BCS G_UP
		LDA #$09
		STA $C2,x
		LDA #$30
		STA $E4,x
		STA $D8,x
		STZ $AA,x
		RTS
G_UP:		DEC $AA,x
		JSL $01801A		; apply y speed
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 9: Going Down Position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

POSITIONDOWN:	LDA $D8,x
		CMP #$70
		BCC KEEP_GOING
		LDA #$0A
		STA $C2,x
		LDA #$FF
		STA $1540,x
		STZ $157C,x
		RTS
KEEP_GOING:	INC $AA,x
		JSL $01801A		; apply y speed
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State A: Electric RAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
RAIN:		LDA $1540,x
		BNE MAYBESHOWER
		LDA #!ORIGINAL_PALETTE
		STA $15F6,x
		JMP NEXT_STATE
MAYBESHOWER:	LDA #!ELEC_PALETTE 
		STA $15F6,x
		LDA $1540,x
		AND #$1F
		BEQ CREATE_BALL
		LDA $E4,x
		CMP #$B0
		BCS CHANGE_DIR
		CMP #$30
		BCC CHANGE_DIR
		BRA NO_CHANGE_DIR
CHANGE_DIR:	LDA $157C,x
		EOR #$01
		STA $157C,x
NO_CHANGE_DIR:	LDA $157C,x
		BEQ SHOWER_RIGHT
		DEC $E4,x
		RTS
SHOWER_RIGHT:	INC $E4,x
		RTS

CREATE_BALL:	JSL $02A9DE             ; \ get an index to an unused sprite slot, RETURN if all slots full
                BMI NOBALL 	         ; / after: Y has index of sprite being generated

		LDA #$08                ; \ set sprite status for new sprite
              	STA $14C8,y             ; /

              	PHX
		LDA #$B1
               	TYX
               	STA $7FAB9E,x
               	PLX

		LDA #$08
		STA $7FAB10,x

		JSL $01ACF9
		LDA $148D
		AND #$0F
		CLC
		ADC #$04
		CLC
              	ADC $E4,x               ; \ set x position for new sprite
            	STA $00E4,y

           	LDA $14E0,x             ;  |
               	STA $14E0,y             ; /

		LDA $D8,x               ; \ set y position for new sprite
               	SEC                     ;  | (y position of generator - 1)
             	SBC #$00                ;  |
             	STA $00D8,y             ;  |
            	LDA $14D4,x             ;  |
          	SBC #$00                ;  |
              	STA $14D4,y             ; /

		PHX                     ; \ before: X must have index of sprite being generated
                TYX                     ;  | routine clears *all* old sprite values...
                JSL $07F7D2             ;  | ...and loads in new values for the 6 main sprite tables
                JSL $0187A7             ;  get table values for custom sprite
		JSL $01ACF9
		LDA $148D
		AND #$01 
             	ORA #$88
             	STA $7FAB10,x
		PLX                     ; / 

		LDA #$10
		STA $1DF9
NOBALL:		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Jelectro GRAPHICS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_DISP:		db $10,$00,$10,$00
Y_DISP:		db $10,$10,$00,$00
TILE:		db $22,$20,$02,$00,$26,$24,$06,$04
TILE_S6:	db $62,$60,$42,$40,$66,$64,$46,$44

GRAPHICS:	JSL !GetDrawInfo
		LDA $14
		LSR A
		LSR A
		LSR A
		AND #$01
		ASL
		ASL
		STA $03
		LDA $15F6,x
		STA $04		
		LDA $C2,x
		CMP #$06
		BEQ STATE_6G
		CMP #$07
		BEQ STATE_6G
			
STATE_0G:	PHX
		LDX #$03
GRAPH_LOOP:	LDA $00
		CLC
		ADC X_DISP,x
		STA $0300,y
	
		LDA $01
		CLC
		ADC Y_DISP,x
		STA $0301,y
		
		PHX
		TXA
		CLC
		ADC $03
		TAX
		LDA TILE,x
		STA $0302,y
		PLX
		
		LDA $04
		STA $0303,y
		
		INY
		INY
		INY
		INY
		DEX
		
		BPL GRAPH_LOOP
	
GRAPH_END:	PLX
		LDY #$02		; Y ends with the TILE size .. 02 means it's 16x16
		LDA #$03		; A -> number of tiles drawn - 1.
					; I drew 2 tiles, so 2-1 = 1. A = 01.

		JSL $01B7B3		; Call the routine that draws the sprite.
		RTS			; Never forget this!

STATE_6G:	PHX
		LDX #$03
GRAPH_LOOP6:	LDA $00
		CLC
		ADC X_DISP,x
		STA $0300,y
	
		LDA $01
		CLC
		ADC Y_DISP,x
		STA $0301,y
		
		PHX
		TXA
		CLC
		ADC $03
		TAX
		LDA TILE_S6,x
		STA $0302,y
		PLX
		
		LDA $04
		STA $0303,y
		
		INY
		INY
		INY
		INY
		DEX
		
		BPL GRAPH_LOOP6
		JMP GRAPH_END



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUBHORZPOS:		LDY #$00
			LDA $D1
			SEC
			SBC $E4,x
			STA $0F
			LDA $D2
			SBC $14E0,x
			BPL RETURN1
			INY
RETURN1:			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_VERT_POS
; This routine determines if Mario is above or below the sprite.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B829
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUBVERTPOS:		LDY #$00
			LDA $D3
			SEC
			SBC $D8,x
			STA $0F
			LDA $D4
			SBC $14D4,x
			BPL RETURN2
			INY
RETURN2:			RTS