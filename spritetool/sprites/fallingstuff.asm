;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

incsrc subroutinedefs_xkas.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; RAM address defines - don't change these
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!RAM_FrameCounter	= $13
			!RAM_FrameCounterB	= $14
			!RAM_ScreenBndryXLo	= $1A
			!RAM_ScreenBndryYLo	= $1C
			!RAM_ScreensInLvl	= $5D
			!RAM_MarioAnimation	= $71
			!RAM_MarioDirection	= $76
			!RAM_MarioSpeedX	= $7B
			!RAM_MarioSpeedY	= $7D
			!RAM_MarioXPos		= $94
			!RAM_MarioXPosHi	= $95
			!RAM_MarioYPos		= $96
			!RAM_SpritesLocked	= $9D
			!RAM_SpriteNum		= $9E
			!RAM_SpriteSpeedY	= $AA
			!RAM_SpriteSpeedX	= $B6
			!RAM_SpriteState	= $C2
			!RAM_SpriteYLo		= $D8
			!RAM_SpriteXLo		= $E4
			!OAM_ExtendedDispX	= $0200
			!OAM_ExtendedDispY	= $0201
			!OAM_ExtendedTile	= $0202
			!OAM_ExtendedProp	= $0203
			!OAM_DispX		= $0300
			!OAM_DispY		= $0301
			!OAM_Tile		= $0302
			!OAM_Prop		= $0303
			!OAM_Tile2DispX		= $0304
			!OAM_Tile2DispY		= $0305
			!OAM_Tile2		= $0306
			!OAM_Tile2Prop		= $0307
			!OAM_TileSize		= $0460
			!RAM_IsBehindScenery	= $13F9
			!RAM_RandomByte1	= $148D
			!RAM_RandomByte2	= $148E
			!RAM_KickImgTimer	= $149A
			!RAM_SpriteYHi		= $14D4
			!RAM_SpriteXHi		= $14E0
			!RAM_Reznor1Dead	= $1520
			!RAM_Reznor2Dead	= $1521
			!RAM_Reznor3Dead	= $1522
			!RAM_Reznor4Dead	= $1523
			!RAM_DisableInter	= $154C
			!RAM_SpriteDir		= $157C
			!RAM_SprObjStatus	= $1588
			!RAM_OffscreenHorz	= $15A0
			!RAM_SprOAMIndex	= $15EA
			!RAM_SpritePal		= $15F6
			!RAM_Tweaker1662	= $1662
			!RAM_Tweaker1686	= $1686
			!RAM_ExSpriteNum	= $170B
			!RAM_ExSpriteYLo	= $1715
			!RAM_ExSpriteXLo	= $171F
			!RAM_ExSpriteYHi	= $1729
			!RAM_ExSpriteXHi	= $1733
			!RAM_ExSprSpeedY	= $173D
			!RAM_ExSprSpeedX	= $1747
			!RAM_OffscreenVert	= $186C
			!RAM_OnYoshi		= $187A
			!RAM_ShakeGrndTimer	= $1887

			!RAM_ExtraBits 		= $7FAB10
			!RAM_CustSpriteNum	= $7FAB9E

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "INIT ",pc
			RTL				; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR Main
			PLB
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Main:			LDA !RAM_SpriteState,x
			BEQ Spawner
			JMP Object


Spawner:		LDA !RAM_SpritesLocked
			BNE Return
			JSL !SubOffScreenX0
			LDY #$1F
			LDA !RAM_ExtraBits,x
			AND #$04
			BEQ ExtraBitClear
			LDY #$3F
ExtraBitClear:		STY $00
			LDA !RAM_FrameCounterB
			AND $00
			BEQ TimeToSpawn
Return:			RTS

TimeToSpawn:		LDY #$09
LoopStart:		LDA $14C8,y
			BEQ SlotFound
			DEY
			BPL LoopStart
			RTS

SlotFound:		LDA #$08
			STA $14C8,y
			LDA !RAM_CustSpriteNum,x
			PHX
			TYX
			STA !RAM_CustSpriteNum,x
			PLX
			LDA !RAM_SpriteXLo,x
			CLC
			ADC #$08
			STA.w !RAM_SpriteXLo,y
			LDA !RAM_SpriteXHi,x
			ADC #$00
			STA.w !RAM_SpriteXHi,y
			LDA !RAM_SpriteYLo,x
			STA.w !RAM_SpriteYLo,y
			LDA !RAM_SpriteYHi,x
			STA.w !RAM_SpriteYHi,y
			PHX
			TYX
			JSL $87F7D2
			JSL $8187A7
			LDA #$88
			STA !RAM_ExtraBits,x
			PLX
			LDA #$01
			STA.w !RAM_SpriteState,y
			JSL $81ACF9
			AND #$0F
			STA $1602,y
			RTS



Object:			JSR SpriteGraphics
			LDA !RAM_SpritesLocked
			BNE Return
			LDA $14C8,x
			CMP #$08
			BNE Return
			JSL !SubOffscreenX0

			; LDA #$40
			; STA !RAM_SpriteSpeedY,x
			; JSL $01801A
			JSL $81802A		; x/y with gravity
			JSL $81A7DC
			RTS

Tilemap:		db $A0,$A0,$A0,$A0,$A0,$C2,$C2,$C2
			db $C2,$C2,$C2,$E6,$E6,$E6,$E6,$8A

SpriteGraphics:		JSL !GetDrawInfo
			LDA $1602,x
			STA $02

			LDA $00
			STA !OAM_DispX,y
			LDA $01
			STA !OAM_DispY,y
			PHX
			LDX $02
			LDA Tilemap,x
			STA !OAM_Tile,y
			PLX
			LDA !RAM_SpritePal,x
			ORA $64
			STA !OAM_Prop,y

			LDA #$00
			LDY #$02
			JSL $81B7B3
			RTS			
