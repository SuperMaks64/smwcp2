;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pile Driver Micro Goomba (Jumping Brick), by SMWEdit
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!ACTSTATUS = $1528	; 0=waiting for mario ; 1=preparing to jump ; 2=jumping
		!PREPTIME = $163E	; decremental timer address used for preparing to jump
		!TIME_UNTIL_JUMP = $27	; amount to set in timer while brick springs
		!JUMP_SPEED = $9F	; jumping speed, works best with an F at the end (originally $AF)
		!DISABLEJUMP = $1558	; time after landing to disable jumping
		!DISABLEDFOR = $30	; amount of frames to disable brick after landing
		!JUMPMAXDIST = $30	; how close mario needs to be to make the brick jump
		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		PRINT "INIT ",pc
		LDA #$0F		; \ set goomba to be sprite's
		STA $C2,x		; / death frame.
		STZ !ACTSTATUS,x		; \  reset timers and
		STZ !PREPTIME,x		;  | address used for
		STZ !DISABLEJUMP,x	; /  sprite's action

		LDA $E4,x
		STA $1504,x
		RTL		

		PRINT "MAIN ",pc			
		PHB
		PHK				
		PLB				
		JSR SPRITE_ROUTINE	
		PLB
		RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_SPEED_TABLE:	db $10,$F0			    ; speeds (right, left)

RETURN1:		RTS

SPRITE_ROUTINE:	JSR SUB_GFX
		LDA $14C8,x		; \  don't execute main
		CMP #$08		;  | sprite code unless
		BNE RETURN1		; /  status is "normal" (8)
		LDA $9D			; \ RETURN if sprites locked
		BNE RETURN1		; /
		JSR SUB_OFF_SCREEN_X0	; only process sprite while on screen             

		LDA $1588,x		; \ check if sprite is touching object side
		AND #%00000011		; /
		BEQ DONT_CHANGE_DIR	; don't change direction if not touching side
		LDA $157C,x		; \
		EOR #$01		;  | flip sprite direction
		STA $157C,x		; /
DONT_CHANGE_DIR:

		STZ $B6,x		; set X speed to zero (as a default)
		LDA !ACTSTATUS,x		; \ brick is jumping?
		CMP #$02		; /
		BNE NOT_IN_JUMP_MODE	; if not, don't execute following code
		LDY $157C,x		; \  set X speed
		LDA X_SPEED_TABLE,y	;  | depending on
		STA $B6,x		; /  direction.
		LDA $1588,x		; \ check to see
		AND #%00000100		; / if brick has landed
		BEQ NOT_LANDED		; if not, don't execute folling code
		LDA #$00		; \ set status for brick waiting
		STA !ACTSTATUS,x		; / for mario to come near
		LDA #$09		; \ play sound effect
		STA $1DFC		; / for brick landing
		LDA #!DISABLEDFOR	; \ prevent brick from executing
		STA !DISABLEJUMP,x	; / for a set amount of frames
NOT_LANDED:
NOT_IN_JUMP_MODE:

		LDA !ACTSTATUS,x		; \ brick is preparing to jump?
		CMP #$01		; /
		BNE NOT_IN_PREP_MODE	; if not, don't execute following code
		LDA !PREPTIME,x		; \  check timer for block to start jumping,
		;CMP #$00		;  | timer is also used as pointer to brick
		BNE NO_START_JUMP	; /  tile Y offsets in the "springing" action
		LDA #$02		; \ set status to jumping
		STA !ACTSTATUS,x		; /
		LDA #!JUMP_SPEED		; \ make brick jump
		STA $AA,x		; /
		JSR SUB_HORZ_POS        ; \
		TYA			;  | go toward mario
		STA $157C,x		; /
NO_START_JUMP:
NOT_IN_PREP_MODE:

		LDA $1588,x		; \ on ground?
		AND #%00000100		; /
		BEQ NOT_IN_WAIT_MODE	; if not, don't execute following code
		LDA !DISABLEJUMP,x	; jump disabled?
		BNE NOT_IN_WAIT_MODE	; if so, don't execute following code
		LDA !ACTSTATUS,x		; \ status = waiting for mario?
		;CMP #$00		; /
		BNE NOT_IN_WAIT_MODE	; if not, don't execute following code
DISTCHECK:	LDA $E4,x		; \  put low and high byte of
		STA $04			;  | sprite X position in order
		LDA $14E0,x		;  | so that 16 bit mode can
		STA $05			; /  work with the X position
		JSR SUB_HORZ_POS	; \ check which side mario is on
		CPY #$00		; /
		BNE RCHECK		; check right side if on right
		BRA LCHECK		; else check left side
RCHECK:		PHP			; \
		REP #%00100000		;  | get the distance between
		LDA $04			;  | the sprite and mario
		SEC			;  | assuming mario is on
		SBC $94			;  | the RIGHT side of
		STA $06			;  | the sprite
		PLP			; /
		BRA COMPARE		; branch to check distance
LCHECK:		PHP			; \
		REP #%00100000		;  | get the distance between
		LDA $94			;  | the sprite and mario
		SEC			;  | assuming mario is on
		SBC $04			;  | the LEFT side of
		STA $06			;  | the sprite
		PLP			; /
COMPARE:		LDA $06			; load distance (difference)
		CMP #!JUMPMAXDIST	; COMPARE to maximum distance
		BCS NO_SET_PREP_MODE	; if too far, don't jump
		LDA $07			; \  don't jump if 100 or
		;CMP #$00		;  | greater, which would
		BNE NO_SET_PREP_MODE	; /  mean there is a high byte
		LDA #$01		; \ set status for doing spring
		STA !ACTSTATUS,x		; / action before jumping
		LDA #!TIME_UNTIL_JUMP	; \ set jump timer
		STA !PREPTIME,x		; /
NO_SET_PREP_MODE:
NOT_IN_WAIT_MODE:

		LDA $1588,x		; \ check if hitting ceiling
		AND #%00001000		; / 
		BEQ NO_BOUNCE_DOWN	; if not, don't execute following code
		LDA $AA,x		; \ check if going up
		CMP #$7D		; /
		BCC NO_BOUNCE_DOWN	; if not, don't execute following code
		LDA #$FF		; \  bounce brick down
		SEC			;  | by subtracting from
		SBC $AA,x		;  | $FF (255)
		STA $AA,x		; /
NO_BOUNCE_DOWN:

RETURN:		JSL $01802A             ; update position based on speed values
		JSL $018032             ; interact with other sprites
		JSL $01A7DC             ; check for mario/sprite contact
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BLOCK_OFFSET:	db $01,$01,$01,$01,$01,$01,$01,$00,$00,$00,$00,$00,$00,$00,$00,$02
		db $02,$02,$02,$02,$02,$02,$02,$04,$04,$04,$04,$04,$04,$04,$04,$05
		db $05,$05,$05,$05,$05,$05,$05,$02

SUB_GFX:		JSL $20CA4F	; after: Y = index to sprite OAM ($300)
				   	;      $00 = sprite x position relative to screen boarder 
				   	;      $01 = sprite y position relative to screen boarder  

		LDA !PREPTIME,x		; \ store timer into $02
		STA $02			; / for future reference
		STZ $03			; zero out $03 in case of no value being set
		LDA !ACTSTATUS,x		; \ check if in springing (jump prep) mode
		CMP #$01		; /
		BNE NOT_IN_PREP_MODE_2	; if not, don't execute following code
		PHX			; \
		LDX $02			;  | store current offset
		LDA BLOCK_OFFSET,x	;  | for springing action
		STA $03			;  | into $03
		PLX			; /
NOT_IN_PREP_MODE_2:

		LDA !ACTSTATUS,x		; \ check if in jumping mode
		CMP #$02		; /
		BNE NOT_IN_JUMP_MODE_2	; if not, don't execute following code
		LDA $AA,x		; \ check if going up
		CMP #$7D		; /
		BCC NOT_2_HIGH		; if not, don't execute following code
		LDA #$02		; \ set it to be two tiles high
		STA $03			; / to expose goomba's "feet"
NOT_2_HIGH:
NOT_IN_JUMP_MODE_2:

		LDA $00                 ; \ set X position of brick
		STA $0300,y		; /

		LDA $01                 ; get Y position of brick
		SEC			; \ subtract 1 to compensate for
		SBC #$01		; / always being 1 pixel lower
		SEC			; \ subtract offset
		SBC $03			; /
		STA $0301,y		; set Y position

		LDA $7FAB10,x
		AND #$04
		BNE GEM_TYPE
		LDA #$C6                ; \ set tile number to [2]A8	
		STA $0302,y		; /
		BRA DONE_BRICK_TILE
GEM_TYPE:	LDA #$C4
		STA $0302,y
DONE_BRICK_TILE:

		LDA $15F6,x             ; get sprite palette info
		ORA $64                 ; add in the priority bits from the level settings
		STA $0303,y             ; set properties

		LDA $7FAB10,x
		AND #$04
		BEQ DONE_PALETTE_CHANGE
		LDA $1504,x
		LSR
		LSR
		LSR
		LSR
		AND #$03
		BEQ PALETTE_A
		CMP #$01
		BEQ PALETTE_B
		CMP #$02
		BEQ PALETTE_C
		LDA $0303,y
		AND #%11110001
		ORA #%00001010
		STA $0303,y
		BRA DONE_PALETTE_CHANGE
PALETTE_C:
		LDA $0303,y
		AND #%11110001
		ORA #%00001000
		STA $0303,y
		BRA DONE_PALETTE_CHANGE
PALETTE_B:
		LDA $0303,y
		AND #%11110001
		ORA #%00000110
		STA $0303,y
		BRA DONE_PALETTE_CHANGE
PALETTE_A:
		LDA $0303,y
		AND #%11110001
		ORA #%00000100
		STA $0303,y
DONE_PALETTE_CHANGE:

		INY			; \  get the index
		INY			;  | to the next slot
		INY			;  | of the OAM by
		INY			; /  incrementing by 4

		LDA $00                 ; \ set X position of microgoomba
		STA $0300,y		; /

		LDA $01                 ; get Y position of the tile
		SEC			; \ subtract 1 to compensate for
		SBC #$01		; / always being 1 pixel lower
		STA $0301,y		; set Y position

		LDA #$E4                ; \ set tile number to [2]AA		
		STA $0302,y		; /

		LDA $15F6,x             ; get sprite palette info
		ORA $64                 ; add in the priority bits from the level settings
		PHA			; \
		LDA $14			;  |
		AND #%00001000		;  |
		STA $02			;  | flip the
		PLA			;  | micro goomba
		PHX			;  | every 8
		LDX $02			;  | frames
		BNE NO_FLIP		;  |
		ORA #%01000000		;  |
NO_FLIP:		PLX			; /
		STA $0303,y             ; set properties

		LDY #$02                ; tiles are 16x16 (02 = 16x16, 00 = 8x8)
		LDA #$01		; it's two tiles (# - 1)
		JSL $01B7B3		; jump to routine to reserve spaces
		RTS



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ROUTINES FROM THE LIBRARY ARE PASTED BELOW
; You should never have to modify this code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, RETURN
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; RETURN

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00				;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
					LDA $94					;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
					SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
					SBC $E4,x				;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
					STA $0F					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
					LDA $95					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
					SBC $14E0,x				;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
					BPL SPR_L16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
					INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
SPR_L16:				RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642


