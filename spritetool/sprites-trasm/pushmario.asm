;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Push Mario Upwards
;;by Joey : ) (Masashi27)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!EXTRA_BITS   = $7FAB10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Initial Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Main Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT_TWO: "MAIN ",pc
PHB
PHK
PLB
JSR SPRITEROUTINE
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Sprite Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITEROUTINE:
JSR GRAPHICS
LDA $14C8,x
CMP #$08
BNE RETURN
LDA $9D
BNE RETURN

LDA $1528,x
CMP #$02
BEQ VERTICAL
CMP #$01
BEQ HORIZONTAL
BRA CHECKCONTACT

HORIZONTAL:
LDA #$90
STA $7B
STZ $7D
LDA $1534,x
CMP #$18
BEQ INCSTATE
INC $1534,x
BRA RETURN

VERTICAL:
LDA #$80
STA $7D
STZ $7B
BRA RETURN

INCSTATE:
INC $1528,x
LDA #$08
STA $1DFC
BRA RETURN

CHECKCONTACT:
JSL $01A7DC
BCC RETURN
LDA #$01
STA $1528,x
LDA #$08
STA $1DFC
RETURN:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;GRAPHICS Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GRAPHICS:
JSL $20CA4F

LDA $157C,x             ; \ $02 = direction
STA $02                 ; /                  

LDA $00                 ; \ tile x position = sprite x location ($00)
STA $0300,y             ; /
                    
LDA $01                 ; \ tile y position = sprite y location ($01)
STA $0301,y             ; /

LDA $15F6,x             ; tile properties xyppccct, format
PHX
LDX $02                 ; \ if direction == 0...
BNE NO_FLIP             ;  |
ORA #$40                ; /    ...flip tile
NO_FLIP:
ORA $64                 ; add in tile priority of level
STA $0303,y             ; store tile properties

LDA #$6C                 ; \ store tile
STA $0302,y             ; /
PLX
INY                     ; \ increase index to sprite tile map ($300)...
INY                     ;  |    ...we wrote 1 16x16 tile...
INY                     ;  |    ...sprite OAM is 8x8...
INY                     ; /    ...so increment 4 times

LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
LDA #$00                ;  | A = (number of tiles drawn - 1)
JSL $01B7B3             ; / don't draw if offscreen
RTS                     ; RETURN

