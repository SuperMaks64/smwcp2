arch spc700-raw

; this contains some extra $1DFA commands 
; just so no one thinks about playing them as sound effects

derp_hack:
	mov	y, #ProcessAPU1SFX>>8	; push return address
	push	y
	mov	y, #ProcessAPU1SFX&$FF
	push	y
	and	a, #$0F
	asl	a
	mov	x, a
	jmp	(derp_ptrs+x)

derp_ptrs:
	dw excmdF0
	dw excmdF1
	dw excmdF2
	dw excmdF3
	dw excmdF4
	dw excmdF5
	dw excmdF6
	dw excmdF7
	dw excmdF8
	dw excmdF9
	dw excmdFA
	dw excmdFB
	dw excmdFC
	;dw excmdFD
	;dw excmdFE
	;dw excmdFF

excmdF0:				; increase global pitch
{
	inc	$43
	inc	!pitch_mod
	ret
}

excmdF1:				; decrease global pitch
{
	dec	$43
	dec	!pitch_mod
	ret
}

excmdF2:				; increase global volume left
{
	mov	a, !volume_left
	inc	a
	mov	!volume_left, a
update_volume_left:
	mov	x, a
	mov	a, volume_values+x
	mov	$F2, #$0C
	mov	$F3, a
	ret
}


excmdF3:				; decrease global volume left
{
	mov	a, !volume_left
	dec	a
	mov	!volume_left, a
	bra	update_volume_left
	
}

excmdF4:				; increase global volume right
{
	mov	a, !volume_right
	inc	a
	mov	!volume_right, a
update_volume_right:
	mov	x, a
	mov	a, volume_values+x
	mov	$F2, #$1C
	mov	$F3, a
	ret
}

excmdF5:				; decrease global volume right
{
	mov	a, !volume_right
	dec	a
	mov	!volume_right, a
	bra	update_volume_right
}

excmdF6:				; invert phase left
{
	mov	a, !volume_left
	cmp	a, #$11
	bcs	.surround
	adc	a, #$11
	mov	!volume_left, a
	bra	update_volume_left

.surround
	sbc	a, #$11
	mov	!volume_left, a
	bra	update_volume_left
}

excmdF7:				; invert phase right
{
	mov	a, !volume_right
	cmp	a, #$11
	bcs	.surround
	adc	a, #$11
	mov	!volume_right, a
	bra	update_volume_right

.surround
	sbc	a, #$11
	mov	!volume_right, a
	bra	update_volume_right
}

excmdF8:				; toggle all channels
{
	eor	$5E, #$FF
	bra	+
}

excmdF9:				; mute all channels (or unmute if they're all already muted)
{
	cmp	$5E, #$FF
	beq	.unmute
	mov	$5E, #$FF
	bra	+
	
.unmute
	mov	$5E, #$00
+	mov	$F2, #$5C
	mov	$F3, $5E
	ret
}

excmdFA:				; increase tempo
{
	!up = #$05
	!down = #$FB
	
	mov	a, !up
	bra	change_tempo
excmdFB:				; decrease tempo
	mov	a, !down
change_tempo:
	push	a
	clrc
	adc	a, $0387
	mov	$0387, a
	pop	a
	clrc
	adc	a, $51
	mov	$51, a
	ret
}

excmdFC:				; toggle sending envelope values to the 5A22
{					; also used as jukebox flag
	mov	a, $038C
	eor	a, #$01
	mov	$038C, a
	mov	$F2, #$0C		; good place to reset global volume too
	mov	$F3, #$7F
	mov	$F2, #$1C
	mov	$F3, #$7F
	mov	a, #$00
	mov	!pitch_mod, a
	mov	a, #$10
	mov	!volume_left, a
	mov	!volume_right, a
	ret
}

; this handles toggling channels upon SNES request ($E0-$EF)
handle_toggle_request:
	and	a, #$07
	mov	x, a
	mov	a, mask_table+x
	eor	a, $5E
	mov	$5E, a
	mov	$F2, #$5C
	mov	$F3, $5E
	jmp	ProcessAPU1SFX

mask_table:
	db $01,$02,$04,$08,$10,$20,$40,$80

; this handles sending envelope data to the SNES when said option is enabled
send_envelopes:
	mov	x, #$07
	mov	y, #$03
.loop
	mov	a, DSP_env+x
	mov	$F2, a
	mov	a, $F3
	and	a, #$70		;E0
	mov	$0166+y, a
	dec	x
	mov	a, DSP_env+x
	mov	$F2, a
	mov	a, $F3
	and	a, #$70		;E0
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	or	a, $0166+y
	mov	$0166+y, a
	dec	x
	dec	y
	bpl	.loop
	ret
	
DSP_env:
	db $08,$18,$28,$38,$48,$58,$68,$78
	
volume_values:
	db $00,$07,$0F,$17,$1F,$27,$2F,$37
	db $3F,$47,$4F,$57,$5F,$67,$6F,$77
	db $7F
	db $00,$F8,$F0,$E8,$E0,$D8,$D0,$C8
	db $C0,$B8,$B0,$A8,$A0,$98,$90,$88
	db $80
	
hax_ram:
	db $00,$00,$00
	