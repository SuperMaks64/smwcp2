#SPC
{
	#author "Yonowaaru"
	#comment "Oriental theme 2"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 w160 t49

$EF $0F $00 $00	;\
$F1 $04 $50 $01	;/ Echo.

#0 @13 v130
$ED $74 $E0
d1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1
r1

#1 @14 v255
$ED $6F $70

o4
r4 $EF $0F $2F $2F <a4>c4<a4
>d2d4e4
f2f4a4
g2g2
e2c4<a4
>g2g4f4
e2d4c4
d2e2
f4<a4>c4<a4
>d4^8d8d4e4
f4^8f8f4a4
g4^8g8g4f4
e2r2
g4^8g8g4f4
e2d4c4
d1
r2d8e8f8g8
a4r4a4g8f8
g2g4f8e8
f8e8d4e4c4
d2<a8b8>c8d8
e2e4d8c8
d2d4<b8g8
a2r2^4^8
>d8e8f8g8a4
a8a4a8a4g4
g8g4g8g4f4
e4c4<a4>d2
d8e8f8g8f4
f8f4f8f8d8e4
e8e4e8e8c8d1
r8
d8e8f8g8a4a8a8
a8g8f4g4g8g8
g8f8e8c8d2
r1^1

#2 ("CPanFlute.brr", $04) v99
$ED $78 $E0 p2,50
 <
r1
y11
d1
y10
f1
y9
g1
y8
e1
y9
g1
y10
e1
y11
d2e2
y12
f1
y11
d1
y10
f1
y9
g1
y8
e2^4r4
y9
g1
y10
e1
y11
d1
y12
r1
a1
y11
g1
y10
f2e2
y9
d1
y8
e1
y9
d1
y10
<a1
y11
r2^4^8>a1
y12
g1
y11
f2
y10
e2d2
y9
r2f1
y8
e1
y9
d1
y10
r2^8
a1
y11
g1
y12
d1
y10
r1^2

#3 @23 v245

r1^2 y10
[@22 a1]14
@22 a2
@22 a4
@22 a1r4
[@22 a1]5
@22 a16
[r1]15                

#amk=1
