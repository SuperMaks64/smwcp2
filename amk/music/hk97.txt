#amk 2

#spc
{
	#title		"Main Theme"
	#game		"Honk Kong 97/SMW"
	#author		"floating munchers"
	#comment	"lmao"
	#length		"1:09"
}

#samples
{
	#optimized
	"hk97.brr"
}

#instruments
{
 	"hk97.brr" $FF $E0 $B8 $02 $1B	; @30
}


#0 t16 w255 o3
@30 v212 y9 q7F $F4 $01 / a+1
