#SPC
{
	#author "Camerin"
	#comment "Bonus theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#6 w160
@8 v245 y10 
$ED $0F $E8
o3
[<a4>a8e8g8f+8e8d8]2
/
[<a4>a8e8g8f+8e8d8]3
<g4>g8d8f+8e8d8c+8
[<a4>a8e8g8f+8e8d8
<g4>g8d8f+8e8d8c+8]2
<a4>a8e8g8f+8e8d8
<a4>a8e8g8b8a4
[<a4>a8e8g8f+8e8d8
<g4>g8d8f+8e8d8c+8]3
<a4>a8e8g8f+8e8d8
<a4>a8e8g8b8a4
d2f+4a4
d2f4b4
c+4e4g+4b4
c4d+4g4a+4
<b2>d4f+8a8
g+4e4c+4<b4
a4>a8e8g8f+8e8d8
<a4>c+8e8a8g+8g4
d2f+4a4
d+2f+4a4
e2f+4g+4
f+4c+4<a+4f+4
>f+2d4<b4
>c+2<b2

#0 w230 t66
[r1]2
/
[r1]2
@11 v240  $ED $1C $88 p20,40
o3
[r4e8a8> {c+4<a4b4}
>e2g8f+8e8d8
e2^4c+8d8
e4c+4d8<b8g4
r4e8a8> {c+4<a4b4}
>e2g8f+8e8d8
e2^4c+8d8
e4g4a2]2
b2a4f+8d8
b4a4f4d4
c+2e4g+8b8
c2d+4g8a+8
<b4>c+4d4f+4
e2<g+2
>e2c+4<a4
>a2g2
f+2g+4a4
>c+2<b4a4
e2c+4e4
f+4e4d4c+4
f+2e4d4
c+2<b2
;
#1 
[r1]2
/
o4
@6 
v160 $ED $14 $E8 p20,40
a1^1
[a1
g1]3
a1^1
[a1
g1]3
a1^1
>d1^1
c+1
c1
<b1
g+1
a1
g1
>d1
d+1
e1
<a+1
b1
e1


#2 
[r1]2
/
o4
@6 v160 $ED $14 $E8 p20,40
e1^1
[c+1
<b1]3
>c+1^1
[c+1
<b1]3
>c+1^1
f+1
f1
e1
d+1
d1^1
c+1^1
f+1^1
a1
c+1
d1
<g+1

#3 
[r1]2
/
@6 v160 $ED $14 $E8 p20,40
o5
a1^1
o4
[e1
d1]3
e1^1
[e1
d1]3
e1^1
a1^1
g+1
g1
f+1
<b1
>e1^1
a1^1
>c+1
<f+1^1
<b1

#5
[r1]2
/
@3 v200
[r1]10
o4
[c+32<a32>]16
<[b32g32]16
>[c+32<a32>]16
<[b32g32]16
>[c+32<a32>]16
<[b32g32]16
>[c+32<a32>]32
o3 @0 q3f v240 $ED $0F $E8
r2a32>d32f+32a32d32f+32a32>d32<f+32a32>d32f+32<a32>d32f+32a32
r2o3a32>d32f32a32d32f32a32>d32<f32a32>d32f32<a32>d32f32a32
r2o3b32>c+32e32g+32c+32e32g+32b32e32g+32b32>c+32<g+32b32>c+32e32
r2o3a+32>c32d+32g32c32d+32g32a+32d+32g32a+32>c32<g32a+32>c32d+32
r2o3f+32b32>d32f+32<b32>d32f+32b32d32f+32b32>d32<f+32b32>d32f+32
e32d32<b32g+32>d32<b32g+32e32b32g+32e32d32g+32e32d32<b32>e32d32<b32g+32>d32<b32g+32e32b32g+32e32d32g+32e32d32<b32
r1
r1
r2>a32>d32f+32a32d32f+32a32>d32<f+32a32>d32f+32<a32>d32f+32a32
r2o3a32>d+32f+32a32d+32f+32a32>c+32<f+32a32>d+32f+32<a32>d+32f+32a32
r2<e32a32>c+32e32<a32>c+32e32a32c+32e32a32>c+32e32c+32<a32e32
r2f+32e32c+32<a+32>e32c+32<a+32f+32>c+32<a+32f+32e32a+32f+32e32c+32
r2<f+32b32>d32f+32<b32>d32f+32b32d32f+32b32>d32<f+32b32>d32f+32
o3d32e32g+32b32e32g+32b32>d32<g+32b32>d32e32<b32>d32e32g+32e32g+32b32>d32<b32>d32e32g+32d32e32g+32b32g+32b32>d32e32
;
#4

[r1]2
/
@4 v160 $ED $1C $E8 p20,40
[r1]10
o4
a4^8g8a4e4
g4f+4e4d4
c+8d8e8c+8d4<b4
g4f+4e4g4
>a4^8g8a4e4
g4f+4e4d4
a4b8>c+8<b4a4
g4e4a2
f+1
f2g2
e1
d+2c2
d1
f+1
e2a2
g1
f+2a2
f+2d+2
>c+1
<a+2f+2
>c+1
<b1

#7
[r1]2
/
o3 @10 v240
@10b4>@29a+8@10f+8r8@10f+8@29a+8@10f+8
<@10b4>@29a+8@10f+8r8@10f+8@29a+8@10f+8
[<@10b8@10b8>@29a+8<@10b8@10b8>@10f+8@29a+8@10f+8]16
[<@10b4>@29a+8@10f+8r8@10f+8@29a+8@10f+8]14                

#amk=1
