;title=Welcome to the Dark Carnival!

#SPC
{
	#author "Lucas"
	#comment "Carnival theme 1"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
	"CStrings.brr"
}


#0
$F0

t51

$EF $0F $30 $30
$F1 $04 $42 $01

;electric piano
#0 @16 v240 o2
$ED $7C $70

{(1)[a4>e4e4<
a4>e4e4<
g+4>d+4d+4<
g+4>d+4d+4<
g4>d4d4<
g4>d4d4<
g+4>d+4d+4<
g+4>d+4d+4<]2
(2)[a4>e4e4<
a4>e4e4<
g+4>d+4d+4<
g+4>d+4d+4<]4
(1)2
(2)4
(1)4}



;trumpet
#1 @4 v125 o3

[r1]24
{e4d+4e4g4f+4g4b4a+4b4}>e2
{d1<b4a4}b1
{e4d+4e4g4f+4g4b4a+4b4}>e2
{f1d4c4}<b1
e2>c2<b2{a+4a4g+4}
d2a2g+1
a2>f+2f2{e4d+4d4}>
d2<a2g+1
e2c2<b2{a+4b4>c4}
d2^4^8^16^32c+32d+1<
a2f2e2{d+4e4f4}
g2^4^8^16^32f+32g+1



;strings 1
#2 v110 o3
@15 ("CStrings.brr", $06)
$ED $E4 $ED

(3)[v110 $E8$FF$60 c+1^1$DD$01$FF$87^1^1 r1^1^1^1 v110]1

[c+2c2<b2>c2c+2c2<{b4a+4b4}>c2]2
(3)1
[>c+2c2<b2>c2c+2c2<{b4a+4b4}>c2<]2
[r1]16



;xylophone
#3 @3 v120 o5
$ED $7E $7A

[r1]8

{[r4f8e8f8r8
r4f8e8f8r8
r4d+8d8d+8r8
r4d+8d8d+8r8
r4f8e8f8f+8f8e8f8e8f8e8d+8d8d+8d8d+8e8f8e8d+8d8d+8r8]6}

[r1]8

{[r4f8e8f8r8
r4f8e8f8r8
r4d+8d8d+8r8
r4d+8d8d+8r8
r4f8e8f8f+8f8e8f8e8f8e8d+8r1^4^8]6}



;strings 2
#7 v105 o3
@15 ("CStrings.brr", $06)
$ED $E5 $ED

[r1]24
[c+2c2<b2>c2
c+2c2<{b4a+4b4}>c2]2
[r1]16



;drums
#5
v160 o3

{[@21 c4 @10 c4c4
r4 @21 c4 @21 c4
r4 @10 c8 @21 c8 @10 c4
@21 c4 @21 c4 @21 c4]16
[@21 c4 r2
r4 @21 c4 @21 c4
r4^8 @21 c8 r4
@21 c4 @21 c4 @21 c4]8}



;hihat
#4
v160 o3

{[@22 c4 @22 c8 @22 c8 @22 c4]96}
                

#amk=1
