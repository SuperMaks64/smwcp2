;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;A Summer Night's Melancholic Forest Air by Lunar Wurmple                     ;
;SEMI-FINISHED                                                                ;
;N-SPC Patch Needed: Yes, HuFlungDu's                                         ;
;Sample Bank Included: No                                                     ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#SPC
{
	#author "Lunar Wurmple"
	#comment "Ghost house theme 2"
	#game "SMWCP2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
	"CChoir.brr"
}

#0 w160 t42	@8	q7f v255 y2

$EF $10 $00 $00
$F1 $02 $32 $00
o2
[d4a8g16a8.g8f8g8
d4a8g16a8.g8f8g8
d4a8g16>c8.<a+8a8a+8
d4g8f16g8.d8c8<a8>]6/
[d4a8g16a8.g8f8g8
d4a8g16a8.g8f8g8
d4a8g16>c8.<a+8a8a+8
d4g8f16g8.d8c8<a8>]6;4+2

#1	@4	q7f v160 y11
p200,100
o2
$ED $75 $F0
[d1
e1
f1
c1]2>
[d1
c1<
a+1
a1>]4
/o3
[d1
c1<
a+1
a1>]6;4+2


#2	@0	q7f v245 y11

o3
$ED $5B $AB
[r1]7
r2.d16e16f16>e16<
[a2d2
g2c2
f2<a+2>
e2c2]4/
[a2d2
g2c2
f2<a+2>
e2c2]6;4+2

#3	@6	q7f v245 y11
o4
[r1]24/
a4.f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>c8<{a4g4f4}
g8>c4<g8>c8.<a8.>c8
[d4.<f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>c8<{a4g4f4}
g8>c4<g8>c8.<a8.>c8]2
d16f16d16c16g16d16c16<a16
g16a16g16f16g16a16d16c16
g16d16c16<a16>a16f16d16f16
g16a16d16f16g16a16d16g16
a16>c16d16c16d16f16g16f16
d16<a+16>g16a16e16c16g16a16
d16e16d16c16d32c32<a32>c32<a32g32a32g32
d32a32>c32f32c32<a32g32f32d32c32f32d32c32<a32g32f32>
d4.f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>c8<{a4g4f4}
g8>c4<g8>c8.<a8.>c8
d1^1^1&<d1

#7	 @21 	q7f v245 y19

o3
[r1]7
r2. @23 a4
[ @26 a8 @21 e8 @22 f8 @21 e8 @29 b8 @21 e8 @22 f8 @21 e8]15
 @26 a8 @21 e8 @22 f8 @21 e8 @29 b8 @21 e8
$EF $10 $46 $46
$F1 $02 $32 $00
 @29 b16 @26 a16[ @21 f16]2
$EF $10 $00 $00
$F1 $02 $32 $00
/
[ @26 a8 @21 e8 @22 f8 @21 e8 @29 b8 @21 e8 @22 f8 @21 e8]24;16+8

#5	@13	q7f v200 y5

o4
@13v200y5
$ED $76 $EB
[f1
e1
d1
c1]4
<@0v170y10
$ED $7F $E0
f4.g16e16{f4e4d4}
e4.f16e16{g4f4e4}
a+4.a16a+16{>d4c4<a+4}
a4.e16g16{a4g4e4}
f2>{d4e4f4}
e2{>c4<a+4e4}
f4.d16<a+16>{g4f4d4}
e4.e8{a4g4e4}
/o3
$ED $7F $E1
@0v255y10
[f1
e1
d1
c1]6;4+2

#6	@13	q7f v200 y4

o4
$ED $76 $EB
[d1
c1<
a+1
a1>]6
/o4
[d1
c1<
a+1
a1>]6;4+2

#4	@13	q7f v200 y6

o4
$ED $76 $EB
[a1
g1
f1
e1]6
/o4
[a1
g1
f1
e1]6;4+2                

#amk=1
