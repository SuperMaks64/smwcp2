#SPC
{
	#author "Kil"
	#comment "Sky level 4"
	#game "SMWCP2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}	

#0 w160		; slap bass
$F0		;
$EF $FF $40 $40 ; reverb
$F1 $05 $03 $00	;

$F5 $DA $12 $C4 $16 $F6 $D9 $12 $2F

$F2 $22 $55 $55	;
@14 t68 y10	;
o1		;
		; transpose

r1
a8 a8>e8<a8>/a8c8e8<f4
f8>c8<f8>f8<a8>c8<d4
d8a8g4g8>d8c4
c8e8<e4g+8c+4
f8f8>c8<g4g8>d8<g+4
g+8b8a4>e8<a8b4
b8>b8<b8b8b8>b8<e8
r2r8e4a4


a8>e8<a8>a8c8e8<f4
f8>c8<f8>f8<a8>c8<d4
d8a8g4g8>d8c4
c8e8<e4g+8c+4
f8f8>c8<g4g8>d8<g+4
g+8b8a4>e8<a8b4
b8>d8e8<e8g+8r8a4
a8a8a8a16r16g4f4

f8>c8<g4g8>d8<c4
c8e8a4a8>e8<a8
g+8g+8>e8d4d8a8c4
c8g8[c8]4<g8
a+8a+8>f8<d+4d+8a+8g+4
g+8>d+8<f4f8>c8<g4
[g8]6>c8
r2r8e4<a4

a8>e8<a8>a8c8e8<f4
f8>c8<f8>f8<a8>c8<d4
d8a8g4g8>d8c4
c8e8<e4g+8c+4
f8f8>c8<g4g8>d8<g+4
g+8b8a4>e8<a8b4
b8>b8<b8b8b8>b8<e8
r2r8e4a4


a8>e8<a8>a8c8e8<f4
f8>c8<f8>f8<a8>c8<d4
d8a8g4g8>d8c4
c8e8<e4g+8c+4
f8f8>c8<g4g8>d8<g+4
g+8b8a4>e8<a8b4
b8>d8e8<e8g+8r8a4
a8a8a8a16r16g4f4

f8>c8<g4g8>d8<c4
c8e8a4a8>e8<a8
g+8g+8>e8d4d8a8c4
c8g8[c8]4<g8
a+8a+8>f8<d+4d+8a+8g+4
g+8>d+8<f4f8>c8<g4
[g8]6>c8
r2r8e4<a4a8>e8<a8>



#1		; lead
@6 y9 v240	;
$DE $29 $0B $40 ;
$EA $2E 	;
o5		;
		;

r1
c2/<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8c8<b4
[b16^32r32]6>e8
r2r8e4c2^8

<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8e8f4
e8d8c8<b16^32r32>e8r8<a4
[a16^32r32]4r4f4

g8a8b4>c8d8<g4
a8b8>c4<e8d8e8
f8e8f8>c4<a8>c8<b2
e2r8
c+8c8c+8g4g+8a+8>c4
c+8d+8<g+4a+8>c8<b4
g8b8>f4e8d8c8
r2r8<b4>c2^8



<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8c8<b4
[b16^32r32]6>e8
r2r8e4c2^8

<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8e8f4
e8d8c8<b16^32r32>e8r8<a4
[a16^32r32]4r4v126f4

g8a8b4>c8d8<g4
a8b8>c4<e8d8e8
f8e8f8>c4<a8>c8<b2
e2r8
c+8c8c+8g4g+8a+8>c4
c+8d+8<g+4a+8>c8<b4
g8b8>f4e8d8c8
r2r8<v240b4>c2^8



#2		; harmony
@1 y11 v245 	;
$DE $24 $0B $40 ;
$EA $2E 	;
o4		;
		;

r1
r8c8e4/e8d4c4
c8f4f8e4d4
e8f8d4e8f8e4
f8e8<a+4>c8c+4
<a4^8>d2<b2
>e4f8e8d+4
d+8a8a8g+8g+8f+8e8
r2r8g+4e4

c8e4e8d4c4
c8f4f8e4d4
e8f8d4e8f8e4
f8e8<a+4>c8c+4
<a4^8>d2<b2
>e4f8g8a4
g8f8e8d8<b8r8>d4
d8d8c8c8<b4a2
b2b2
a4a8g8a8
g8g8g8>f2e2
c2r8
<a+4^8>d+2c2
c4c+8d+8d4
d8f8f8e8e8d8e8
r2r8g+4c4


c8e4e8d4c4
c8f4f8e4d4
e8f8d4e8f8e4
f8e8<a+4>c8c+4
<a4^8>d2<b2
>e4f8e8d+4
d+8a8a8g+8g+8f+8e8
r2r8g+4e4

c8e4e8d4c4
c8f4f8e4d4
e8f8d4e8f8e4
f8e8<a+4>c8c+4
<a4^8>d2<b2
>e4f8g8a4
g8f8e8d8<b8r8>d4
d8d8c8c8<b4a2
b2b2
a4a8g8a8
g8g8g8>f2e2
c2r8
<a+4^8>d+2c2
c4c+8d+8d4
d8f8f8e8e8d8e8
r2r8g+4c4 c8e4


#3		; percussion
@2 v210		;
y10 q4f $DF	;

@21c4c4c4c8 @29c8
r8 @21c8 @29c8 @21c8/
[r8 @21c8 @29c8 @21c8]6r8 @21c8 @29c4
@21c8 c8 @29c8 @21c8 [r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @21c8 @29c8 @21c8 @29c8 @29c8
r8 @21c8 @21c8 @21c8 @21c8  @15e8  @29c4

[r8 @21c8 @29c8 @21c8]7r8 @21c8 @29c4
@21c8 c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8 @21c8 @29c8 @15e8 @29c4
@29c8 @29c8 @29c8 @29c8 @21c4 @21c8

[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8
@21c8 @21c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]2
r8 @21c8 @29c16 @29c16  @15c8
@21c8 @21c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8 @21c8 @29c8 @29c8 @15c8
@21c4c4c8 @29c4 @21c8

r8 @21c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]6r8 @21c8 @29c4
@21c8 c8 @29c8 @21c8 [r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @21c8 @29c8 @21c8 @29c8 @29c8
r8 @21c8 @21c8 @21c8 @21c8  @15e8  @29c4

[r8 @21c8 @29c8 @21c8]7r8 @21c8 @29c4
@21c8 c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8 @21c8 @29c8 @15e8 @29c4
@29c8 @29c8 @29c8 @29c8 @21c4 @21c8

[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8
@21c8 @21c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]2
r8 @21c8 @29c16 @29c16  @15c8
@21c8 @21c8 @29c8 @21c8
[r8 @21c8 @29c8 @21c8]3
r8 @21c8 @29c8 @29c8 @21c8 @29c8 @29c8 @15c8
@21c4c4c4c8 @29c4 @21c8 @29c8 @21c8

#7		; bells
@2 y11 v170	;
o3		;
		;

r8c8d8e8g8b8e8b8
>c2/v170r2
[r1]6
r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]14
r8<b8>d+8f+8g+8b4>c2r8r2
[r1]6
<r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]6
v179
o3
>r2r4r8a8
a8e8c8g8d8<b8>e8g4
e8g8e4e8<b8>e8
c8<a8e8>f4c8<a8>e2
<b2r8
>c+8<a+8f8>g4d+8<a+8>g4
d+8<a+8>f4c8<g+8>d4<b8g8>f4e8d8c8
<r8c8d8e8g8b8e8 >c2r8



#5		; bells echo
@2 y7 v160	;
o3		;
		;

r16r8c8d8e8g8b8e8b8
>r4^8^16/v160r2
[r1]6
r16
r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]14
r8<b8>d+8f+8g+8b4>c2r8r2
[r1]6
<r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]6
v150
o3
>r2r4r8a8
a8e8c8g8d8<b8>e8g4
e8g8e4e8<b8>e8
c8<a8e8>f4c8<a8>e2
<b2r8
>c+8<a+8f8>g4d+8<a+8>g4
d+8<a+8>f4c8<g+8>d4<b8g8>f4e8d8c8
<r8c8d8e8g8b8e8 >c4^8^16r8



#6		; bells echo 2
@0 y10 v160	;
o3		;
		;

r8c8d8e8g8b8e8b8
>c2/r2
[r1]6
v160
r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]14
r8<b8>d+8f+8g+8b4>c2r8r2
[r1]6
<r8<f+8a8b8>d8e8<b8>c2r8r2
[r1]6
v170
o3
>r2r4r8a8
a8e8c8g8d8<b8>e8g4
e8g8e4e8<b8>e8
c8<a8e8>f4c8<a8>e2
<b2r8
>c+8<a+8f8>g4d+8<a+8>g4
d+8<a+8>f4c8<g+8>d4<b8g8>f4e8d8c8
<r8c8d8e8g8b8e8 >c2r8




#4		; lead echo
@6 y12 v188	;
$DE $29 $0B $40 ;
$EA $2E 	;
o5		;
		;

r1
c2/<b8^16>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8c8<b4
[b16^32r32]6>e8
r2r8e4c2^8

<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8e8f4
e8d8c8<b16^32r32>e8r8<a4
[a16^32r32]4r4f4

g8a8b4>c8d8<g4
a8b8>c4<e8d8e8
f8e8f8>c4<a8>c8<b2
e2r8
c+8c8c+8g4g+8a+8>c4
c+8d+8<g+4a+8>c8<b4
g8b8>f4e8d8c8
r2r8<v188b4>c2^8



<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8c8<b4
[b16^32r32]6>e8
r2r8e4c2^8

<b8>c8<b8a4
[a16^32r32]3b8>c8d8<f4
g8a8b4>c8d8<g4
a8g8e4d+8e4
f8g8a8b4>c8d8<g4
a8b8>c4d8e8f4
e8d8c8<b16^32r32>e8r8<a4
[a16^32r32]4r4v100f4

g8a8b4>c8d8<g4
a8b8>c4<e8d8e8
f8e8f8>c4<a8>c8<b2
e2r8
c+8c8c+8g4g+8a+8>c4
c+8d+8<g+4a+8>c8<b4
g8b8>f4e8d8c8
r2r8<b4v188>c2^16

                

#amk=1
