
#SPC
{
	#author "Masashi27"
	#comment "Factory theme 3"
	#game "SMWCP2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 w160 t55 q7F @14 v210 $EF $FF $3F $3F $F1 $04 $2C $00 $ED $49 $74 $F4 $05 

o2 [r1]4 [e8>g8<e16>d8<e16>c+8<e16>d8<e16>e8<]8 / v210 [e8>g8<e16>d8<e16>c+8<e16>d8<e16>e8<]10
 c+8>c+8<c+16g+8c+16  c+8>c+8<c+16g+8c+16 < b8>b8<b16>f+8<b16 b8>b8<b16>f+8<b16 > *2
 c+8>c+8<c+16g+8c+16  c+8>c+8<c+16g+8c+16 d8>d8<d16a8d16 d+8>d+8<d+16a+8d+16 *10
 c+8>c+8<c+16g+8c+16  c+8>c+8<c+16g+8c+16 < b8>b8<b16>f+8<b16 b8>b8<b16>f+8<b16 > *2
c+8>c+8<c+16g+8c+16  c+8>c+8<c+16g+8c+16 d8>d8<d16a8d16 e8>e8<f+16>c+8<f+16 
 g8>g8<g16>d8<g16 a8>a8<a16>e8<a16 e8>e8<e16b8e16 f+8>f+8<f+16>c+8<f+16
g8>g8<g16>d8<g16 a8>a8<a16>e8<a16 [e8>e8<e16b8e16]2 g8>g8<g16>d8<g16 a8>a8<a16>e8<a16 e8>e8<e16b8e16
f+8>f+8<f+16>c+8<f+16 g8>g8<g16>d8<g16 a8>a8<a16>e8<a16 [e8>g8<e16>d8<e16>c+8<e16>d8<e16>e8<]3
e16r8 v180 e16r8 v150 e16r8 v120 e16r8 r4

#1 q7F @17 v210 $ED $44 $73

o4 r1r1 y0 [b8 $DD $00 $9A $15 ^8] y20 v190 * y0 v170 * y20 v150 * y0 v130 * y20 v110 * y0 v90 * y20 v70 * y10
@14 v200 $ED $49 $74 o2 [r1]4  [b8>>d8<<b16>a8<b16>a8<b16>a8<b16>b8<]4 / v200 [b8>>d8<<b16>a8<b16>a8<b16>a8<b16>b8<]10
>c+8g+8c+16e8c+16 c+8g+8c+16e8c+16 < b8>f+8<b16>d8<b16 b8>f+8<b16>d8<b16 *2
>c+8g+8c+16e8c+16 c+8g+8c+16e8c+16 d8a8d16f+8d16 d+8a+8d+16g8d+16 *10
c+8g+8c+16e8c+16 c+8g+8c+16e8c+16 < b8>f+8<b16>d8<b16 b8>f+8<b16>d8<b16> *2
c+8g+8c+16e8c+16 c+8g+8c+16e8c+16 d8a8d16f+8d16 e8b8f+16a8f+16 <
>g8>d8<g16b8g16 a8>e8<a16>c+8<a16 e8b8e16g8e16
f+8>c+8<f+16a8f+16  g8>d8<g16b8g16 a8>e8<a16>c+8<a16 [e8b8e16g8e16]2  g8>d8<g16b8g16 a8>e8<a16>c+8<a16 e8b8e16g8e16
f+8>c+8<f+16a8f+16 g8>d8<g16b8g16 a8>e8<a16>c+8<a16 < [b8>>d8<<b16>a8<b16>a8<b16>a8<b16>b8<]3
b16r8 v170 b16r8 v140 b16r8 v110 b16r8 r4

#2 q7F @0 $ED $79 $A0

[r1]12 / v245 y10 [r1]8 o4 e16f16e1^4 e8f8^16e16^8d8 c+1<b1> e16f16e16f16e1^8 e8f8^16e16^8d8c+1d2d+2 [r1  y20 e16r8 y17 e16r8 y14 e16r8 y11 e16
r8 y7 e16r16 y4 e16r16 r1 y1 e16r8 y4 e16r8 y7 e16r8 y11 e16 r8 y14 e16r16 y17 e16r16]2 y10 e16f16e1^4 e8f8^16e16^8 d8 c+16d16c+4c+16d16c+4
c+16d16c+8<b16>c+16<b4b16>c+16<b4b16>c+16d16e16 r8 e8^16f16^8e2 r8 e16d16e16d16e8f8^16e16^8d8c+1d2e4f+4 g4f+8g8a4g8f+8r8
e8f+8e8d4e8f+8 g4f+8g8a4b8>d8 y20 e16r8 y17 e16r8 y14 e16r8 y11 e16 r8 y7 e16r16 y4 e16r16 y10 < g4a8g8f+4g8f+8r8e8f+8e8d4e8f+8g4f+8e8
f+8^16e16^8d8e1r1 $ED $42 $DC e1 > $ED $79 $A0  y20 e16r8 v215 y0 e16r8 v185 y20 e16r8 v155 y0 e16r8 r4

#3 q7F @8 v230 $ED $0F $EF

o3 [r1]12 /  [e16r16e16e16 e16r16e16e16 e16e16r16e16 e16r16e16e16]10
c+16r16c+16c+16 c+16r16c+16c+16 c+16c+16r16c+16 c+16r16c+16c+16<b16r16b16b16 b16r16b16b16 b16b16r16b16 b16r16b16b16>
*2 c+16r16c+16c+16 c+16r16c+16c+16 c+16c+16r16c+16 c+16r16c+16c+16 d16r16d16d16 d16r16d16d16 d+16d+16r16d+16 d+16r16d+16d+16
*10 c+16r16c+16c+16 c+16r16c+16c+16 c+16c+16r16c+16 c+16r16c+16c+16<b16r16b16b16 b16r16b16b16 b16b16r16b16 b16r16b16b16>
r8 e8^16f16^8e2 * c+16r16c+16c+16 c+16r16c+16c+16 c+16c+16r16c+16 c+16r16c+16c+16 d16r16d16d16 d16r16d16d16
e16r16e16e16 f+16r16f+16f+16  [g16r16g16g16 g16r16g16g16 a16a16r16a16 a16r16a16a16 e16r16e16e16 e16r16e16e16
f+16f+16r16f+16 f+16r16f+16f+16]1 g16r16g16g16 g16r16g16g16 a16a16r16a16 a16r16a16a16 e16r8e16r8e16r8e16r8e16r16e16r16 *
g16r16g16g16 g16r16g16g16 a16a16r16a16 a16r16a16a16 [e16r16e16e16 e16r16e16e16 e16e16r16e16 e16r16e16e16]3
e16r8e16r8e16r8e16r8r4

#5 q7F v230

[o2 @10 c8 o3 @22 c16c16 o2 @10 c8 o3 @22 c16c16]23 o2 @10 c8 o3 @22 c16c16 @29 b16a16r16a16/
[o2 @10 c8 o3 @22 c16c16 @29 b8 @22 c16c16 o2 @10 c16 o3 @22 c16r16c16 @29 b8 @22 c16c16]15
o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16 @29 b16a16 *12
r8 o2 @10 c8^16 o3 @29 b16^8 o2 @10 c4^8 o3 @29 b16b16 *2
o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16 @29 b16a16 *3
o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16c16 @29 b16 @22 c16c16 o2 @10 c16 o3 @22 c16 @29 b16a16 *6
b16r8b16 r8b16r8 b16r8 b16a16g16f16

#4 q7F @1 v155

o4 [r1]12  / [y0 e8> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8<]8 [r1]8
[y0 e8> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8< y0 e16> y20 e8<]8 [r1]19                

#amk=1
