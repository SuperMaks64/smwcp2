;This block will spawn a donut lift sprite
;based on mikeyk's code, adapted by Davros
;act like 130

db $42
JMP Return : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP MarioAbove : JMP Return : JMP Return

;custom sprite to generate
!CUSTOM_SPRITE = $A3

MarioAbove:         PHX                     ; push x
                    JSL $02A9E4             ; \ get an index to an unused sprite slot, return if all slots full
		    BMI Pull_x              ; / after: y has index of sprite being generated
		    TYX                     ; tranfer y to x

                    LDA #$08                ; \ set sprite status
		    STA $14C8,x             ; /
		    LDA #!CUSTOM_SPRITE     ; \ set custom sprite number
		    STA $7FAB9E,x           ; /
		    JSL $07F7D2		    ; reset sprite properties
		    JSL $0187A7             ; get table values for custom sprite
		    LDA #$88                ; \ mark as initialized
		    STA $7FAB10,x           ; /

		    LDA $98                 ; \ setup block properties
                    AND #$F0                ;  |
		    STA $D8,x               ;  |
		    LDA $99                 ;  |
		    STA $14D4,x             ;  |
		    LDA $9A                 ;  |
                    AND #$F0                ;  |
		    STA $E4,x               ;  |
		    LDA $9B                 ;  |
		    STA $14E0,x             ; /

                    LDA #$02                ; \ set blank block
                    STA $9C                 ; /
                    JSL $00BEB0             ; Map16 block routine

Pull_x:             PLX                     ; pull x
Return:		    RTL                     ; return