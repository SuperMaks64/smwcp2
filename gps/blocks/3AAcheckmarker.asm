db $42
JMP Mario : JMP Mario : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Mario:		LDA $0DA1
		CMP #$0F
		BEQ ClearRtn
		REP #$20
		LDA $94		;Mario wraps back to the start.
		AND #$11FF
		STA $94
		LDA $1462	;Layer 1's X values go back to the start.
		AND #$11FF
		STA $1462
		LDA $1466	;Layer 2's X values go back to the start.
		AND #$11FF
		STA $1466		
		SEP #$20
		LDA #$2A	;Player correct sound when right path is made.
		STA $1DFC 
		RTL
ClearRtn:	LDA #$29	;Player correct sound when right path is made.
		STA $1DFC 
Return:		RTL