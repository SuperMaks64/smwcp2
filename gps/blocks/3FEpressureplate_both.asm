;Activates nearest laser shooter to both sides of the player OR sprite
;NOTE: Proximity detection isn't perfect!
!CustomShooterNum = $CE		;Number of the custom shooter

db $42
JMP MActivate : JMP MActivate : JMP MActivate
JMP SActivate : JMP SActivate : JMP Return : JMP Return
JMP MActivate : JMP MActivate : JMP MActivate

SActivate:
LDA #$69
STA $07
BRA Activate
MActivate:
STZ $69
Activate:
STZ $05
STZ $06
LDA #$FF		;\
STA $04			;/Address which holds index of closest shooter so far
STA $02			;\address which holds closest Xpos so far
STA $03			;/
LDX #$0B
-
LDA $1783,x
ORA #$40
CMP.b #!CustomShooterNum+$01
BEQ .foundone
.continue
DEX
BPL -

;MOVING ON
LDA $04			;\
BMI +		;/if index of closest shooter is #$FF
TAX
;So if we have an index...
;activate shooter. x = shooter index of closest one
LDA #$80
STA $17AB,x
+
RTL

.foundone
LDA $7FFFFF
LDA $17A3,x		;shooter x high
XBA
LDA $179B,x		;shooter x low
REP #$20
STA $00			;= shooter xpos
SEP #$20
LDA $07
CMP #$69
BNE .xtouchofmario
PHX
LDX $15E9
LDA $14E0,x		;sprite xpos
XBA
LDA $E4,x
PLX
BRA +
.xtouchofmario
REP #$20
LDA $D1			;player xpos
+
REP #$20
SEC
SBC $00
BCS +
EOR #$FFFF
INC A
+
STA $05			;XDIFF

SEP #$20

LDA $1793,x		;shooter x high
XBA
LDA $178B,x		;shooter x low
REP #$20
STA $00			;= shooter xpos
SEP #$20
LDA $07
CMP $69
BNE .ytouchofmario
PHX
LDX $15E9
LDA $14D4,x		;sprite ypos
XBA
LDA $D8,x
PLX
.ytouchofmario
REP #$20
LDA $D3			;player ypos
+
REP #$20
SEC
SBC $00
BCS +
EOR #$FFFF
INC A
+
CLC
ADC $05
STA $05			;XYDIFF

LDA $02
CMP $05
BCC +
LDA $05
STA $02			;$02 = Xdis
STX $04
+
SEP #$20
JMP .continue

Return:
RTL