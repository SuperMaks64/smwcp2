;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;This block will move Mario to the right by 1 pixel
;;every other frame as long as his Y-Speed = 10
;;
;;The tile should act like Tile 25.
;;
;;                                       -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Mario:
	LDA $7D			;\  If Mario's Y-speed
	CMP #$10		; | is stationary
	BNE Return		;/  then branch

	LDA $14			;\ Every other frame
	AND #$01		;/
	BNE Return
	LDA $94			;\
	CLC			; |
	ADC #$01		; | Move 1 pixel right
	STA $94			; |
	LDA $95			; |
	ADC #$00		; |
	STA $95			;/
Return:
	RTL