db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP MarioFireBall : JMP MarioFireBall : JMP MarioFireBall

MarioAbove:
MarioBelow:
MarioSide:
MarioCape:
MarioFireBall:
RTL

SpriteV:
SpriteH:

LDA $18C6
CMP #$00
BNE Break
INC $18C6
RTL

Break:
STZ $18C6
LDA $0A		;\ 
AND #$F0	;| Update the position
STA $9A		;| of the block
LDA $0B		;| so it doesn't shatter
STA $9B		;| where the players at
LDA $0C		;|
AND #$F0	;|
STA $98		;|
LDA $0D		;|
STA $99		;/
LDA #$02 	; Replace block with blank tile
STA $9C
JSL $00BEB0
PHB
LDA #$02
PHA
PLB
LDA #$00
JSL $028663 	;breaking effect
PLB
RTL