;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;This block will move Mario to the right or left
;;by 1 pixel every other frame depending on the
;;setting of Bit 3 found at $7FC0FC as long as
;;his Y-Speed = 10
;;
;;The tile should act like Tile 25.
;;
;;Based on some code by Decimating DJ.
;;                                       -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Mario:
	;LDA $7D			;\  If Mario's Y-speed
	;CMP #$11		; | is stationary
	;BPL Return		;/  then branch

	REP #$20		;\
	LDA $7FC0FC		; |
	AND #$0008		; | Custom Trigger Check
	BEQ Reverse		; |
	SEP #$20		;/

	LDA $14			;\ Every other frame
	AND #$01		;/
	BNE Return
	LDA $94			;\
	CLC			; |
	ADC #$01		; | Move 1 pixel right
	STA $94			; |
	LDA $95			; |
	ADC #$00		; |
	STA $95			;/
Return:
	RTL
Reverse:
	SEP #$20
	LDA $14			;\ Every other frame
	AND #$01		;/
	BNE Return
	LDA $94			;\
	SEC			; |
	SBC #$01		; | Move right 1 pixel
	STA $94			; |
	LDA $95			; |
	SBC #$00		; |
	STA $95			;/
	RTL