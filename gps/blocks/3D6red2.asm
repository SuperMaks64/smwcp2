;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Red Gem Activated Block: This switch will alternate between two
;differing conditions based on Bit 0 of the Custom Trigger
;found at $7FC0FC. Acts like 130. Based on code by Decimating DJ.
;                                                     -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

;If Custom Trigger 0 is on, This is solid
;by Decimating DJ

JMP Code : JMP Code : JMP Code : JMP Code : JMP Code : JMP Code : JMP Code : JMP Code : JMP Code : JMP Code

Code:
	REP #$20
	LDA $7FC0FC
	AND #$0001
	BEQ Jump
	SEP #$20
	LDY #$00
	LDA #$25
	STA $1693
Jump:
	SEP #$20
	RTL