db $42
JMP Mario : JMP Mario : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Mario:	LDA $1493	; If you've already activated the exit, stop.
	BNE Return
	LDA $1DEA	; Check which event would normally be activated
	CLC		; Prepare to add.
	ADC #$02	; Add 2 to the exit number.
	STA $1DEA	; Store that exit number.
	LDA #$FF	; End the level
	STA $1493	; 
	LDA #$0C	; Level End Sound Music.
	STA $1DFB	; Store Sound Music Number.
Return:	RTL		; Return