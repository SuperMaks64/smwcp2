300	300Teleport.asm

301	301Kill.asm
302	302MarioOnlyPass.asm
303	303SpritesOnlyPass.asm
304	304IceBlock.asm
305	305OffSolid.asm
306	306OnSolid.asm
307	307LeftSlip.asm
308	308MidSlip.asm
309	309RightSlip.asm
30A	30ALRSlip.asm
30B	30BBluePOWQuarter.asm
30C	30CSilverPOWQuarter.asm
30D	30DBLUEPowPass.asm
30E	30ESILVERPowPass.asm
30F	30Fwaterice.asm
310	310ledgevine.asm
311	311noyoshi.asm
312	312slipsolidon.asm
313	313slipsolidoff.asm
314	314CrackIceL.asm
315	315CrackIceR.asm
316	316KillInside.asm
317	317Door.asm

31A	31AConveyerLeft.asm
31B	31BConveyerRight.asm
31C	31CConveyerAlternate1.asm
31D	31DConveyerAlternate2.asm
31E	31EHurtManual1.asm
31F	31FHurtManual2.asm


320	320Button.asm
321	320Button.asm
322	322violentsand.asm
323	323Button.asm
324	324disengage.asm
325	325violentsand2.asm
326	326KillMario.asm
327	327HurtTop.asm
328	328bounce.asm
329	329wind_down.asm
32A	32Aslopefix.asm
32B	32BSprite_Kill.asm
32C	32CLeftMid.asm
32D	32DLeftTop.asm
32E	32ERightMid.asm
32F	32FRightTop.asm


330	330SolidForPiranhas.asm
331	331QuicksandNorm.asm	
332	332QuicksandPit.asm
333	333Fragile.asm

335	335shootstraightup.asm
336	336shootstraightright.asm
337	337shootstraightup50.asm
338	338shootstraightright50.asm
339	339shootstraightuponoff.asm
33A	33Ashootstraightupanimated.asm
33B	33Bshootstraightleft.asm
33C	33Cshootstraightleft50.asm
33D	33Dshootstraightdown.asm
33E	33Espritekill.asm
33F	33FQuicksandNorm2.asm


340	340spike.asm
341	341spike_sideways.asm
342	342spike_upsidedown.asm
343	343fragile.asm
344	344drum1.asm
345	345drum2.asm
346	346laser1.asm
347	347laser2.asm
348	348IceTopLeft.asm
349	349IceTopRight.asm
34A	34AIceBottomLeft.asm
34B	34BIceBottomRight.asm
34C	34Cmagiciancoin.asm
34E	34ENorvegHurtPassThrough.asm
34F	31Espike.asm


350	350sand.asm
351	351LedgeOverWater.asm
352	MOVE3.asm
353	353waterbubble.asm
354	354ledge1.asm
355	355ledge2.asm
356	356ledge3.asm
357	357ledge4.asm
358	358HorE.asm
359	359HorD.asm
35A	331QuicksandNorm.asm
35B	35Bsticky.asm
35C	35Cslide.asm

35E	35Ekeyblock.asm


360	360animhurt1.asm
361	361animhurt2.asm
362	362animhurt3.asm
363	363animhurt4.asm
364	364LeftwardsOnly.asm
365	365LeftwardsOnlyCorner.asm
366	366rightcurrent.asm
367	367leftcurrent.asm
368	368upcurrent.asm
369	369downcurrent.asm
36A	36Awindup.asm
36B	36Bshatterifspriteshakeslayer1.asm
36C	36Cteleportifhighestspeed.asm
36D	36Djumpblank.asm
36E	36Ejumpspike.asm
36F	36FPlatform.asm

370	370blockmove.asm
371	371sun.asm
372	372shade.asm

375	375CustomTrigger0Activate.asm
376	376CustomTrigger0Deactivate.asm
377	377Oneshot01Activate.asm

37F	37FSpringRoom.asm


380	380BigDrum.asm
381	381noyoshi.asm
382	382shatter.asm
383	383keyblock.asm

384	384_ceiling_beetle_block.asm

385	385donutblk.asm
386	386brickv13.asm
387	387bounceSound1.asm
388	388bounceSound2.asm
389	389bounceSound3.asm
38A	38Athunderbell.asm

38C	38CP_teleport.asm

390	390Button1.asm
391	391Button2.asm
392	392Button3.asm
393	393Button4.asm
394	394Button5.asm
395	395Button6.asm
396	396Button7.asm
397	397Button8.asm
398	398Button9.asm
399	399CartGate1.asm
39A	39ACartGate2.asm
39B	39BCartGate3.asm
39C	39Cbackmario.asm
39D	39Dbehindwalls.asm
39E	39Efrontwalls.asm
39F	39Finsideledge.asm


3A0	3A0breakifshell.asm
3A1	3A1breakifrshell.asm
3A2	3A2breakifgshell.asm
3A3	3A3breakifyshell.asm
3A4	3A4breakifbshell.asm
3A5	3C1yoshi.asm
3A6	3A6check1.asm
3A7	3A7check2.asm
3A8	3A8check3.asm
3A9	3A9check4.asm
3AA	3AAcheckmarker.asm
3AB	3ABcheckclear.asm
3AC	3ACswitch1.asm
3AD	3ADswitch2.asm
3AE	3AEswitch3.asm
3AF	3AFswitch4.asm


3B0	3B0cleardoor.asm
3B1	3B1doormark1.asm
3B2	3B2doormark2.asm
3B3	3B3doormark3.asm
3B4	3B4doormark4.asm
3B5	3B5fourcornersdoor.asm
3B6	3B6smallmarioonly.asm


3C0	3C0Lantern.asm

3C2	3C2sushi.asm
3C3	3C3MathBlock0.asm
3C4	3C4MathBlock1.asm
3C5	3C5MathBlock2.asm
3C6	3C6MathBlock3.asm
3C7	3C7MathBlock4.asm
3C8	3C8MathBlock5.asm
3C9	3C9MathBlock6.asm
3CA	3CAMathBlock7.asm
3CB	3CBMathBlock8.asm
3CC	3CCMathBlock9.asm
3CD	3CDAnswerSubmit.asm
3CE	3CEfoultele.asm
3CF	3CFplacer.asm

3E0	3E0TrapdoorOff.asm
3E1	3E1TrapdoorOn.asm
3E2	3E2ExitAdder.asm
3E3	3E3revYellowBlock.asm
3E4	3E4revGreenBlock.asm
3E5	3E5revRedBlock.asm
3E6	3E6revBlueBlock.asm
3E7	3E7_3EANorvegHelmet.asm
3E8	3E7_3EANorvegHelmet.asm
3E9	3E7_3EANorvegHelmet.asm
3EA	3E7_3EANorvegHelmet.asm

3F0	3F0lavaon.asm
3F1	3F1vineoff.asm
3F2	3F2wateron.asm
3F3	3F0lavaon.asm
3F4	3F4invertedcloud.asm
3F5	3F5fire_block.asm
3F6	3F6Grinder.asm

3FC	3FCHurtOff.asm
3FD	3FD1FrameKill.asm
3FE	3FEpressureplate_both.asm
3FF	3FFHurtOn.asm

130C	1upleftcoin.asm
130D	1uprightcoin.asm
130E	1downleftcoin.asm
130F	1downrightcoin.asm

131C	2upleftcoin.asm
131D	2uprightcoin.asm
131E	2downleftcoin.asm
131F	2downrightcoin.asm

132C	3upleftcoin.asm
132D	3uprightcoin.asm
132E	3downleftcoin.asm
132F	3downrightcoin.asm

134C	checkpoint0.asm
134D	checkpoint1.asm
134E	checkpoint2.asm

148E	148Eevent.asm

2890	34ENorvegHurtPassThrough.asm
2891	34ENorvegHurtPassThrough.asm
2892	34ENorvegHurtPassThrough.asm

2899	34ENorvegHurtPassThrough.asm
289A	34ENorvegHurtPassThrough.asm
289B	34ENorvegHurtPassThrough.asm

2AA9	Cart.asm
2AAA	Cart.asm
2AAB	Cart.asm
2AAC	Cart.asm
2AAD	Cart.asm
2AAE	Cart.asm
2AAF	Cart.asm

2AB9	Cart.asm
2ABA	Cart.asm
2ABB	Cart.asm
2ABC	Cart.asm
2ABD	Cart.asm
2ABE	Cart.asm
2ABF	Cart.asm

2AC9	Cart.asm
2ACA	Cart.asm
2ACB	Cart.asm
2ACC	Cart.asm
2ACD	Cart.asm
2ACE	Cart.asm
2ACF	Cart.asm

2AD9	Cart.asm
2ADA	Cart.asm
2ADB	Cart.asm
2ADC	Cart.asm
2ADD	Cart.asm
2ADE	Cart.asm
2ADF	Cart.asm

2AE7	OnCart.asm
2AE8	OnCart.asm
2AE9	Cart.asm
2AEA	Cart.asm
2AEB	Cart.asm
2AEC	Cart.asm
2AED	Cart.asm
2AEE	Cart.asm
2AEF	Cart.asm

2AF9	Cart.asm
2AFA	Cart.asm
2AFB	Cart.asm
2AFC	Cart.asm
2AFD	Cart.asm
2AFE	Cart.asm
2AFF	Cart.asm

2D90	2D90EventDoors.asm

2DC8	2DC8RttFDoorA.asm
2DC9	2DC9RttFDoorB.asm
2DCA	2DCApurplecoin.asm
2DCB	2DCBRttFSpike.asm

2DD3	2DD3message2.asm
2DD4	2DD4doortargets.asm
2DD6	2DD6flagpole.asm
2DD7	2DD7failuretele.asm

2DE2	2DE2RttFDoorC.asm
2DE3	2DE3doorcoins.asm

34BA	34BAbellflower.asm
34BB	34BBbellmush.asm
34BC	34BCbellcape.asm
34BD	34BDbell1up.asm

34D8	34D8bell5coin.asm

3649	3D4YellowGemActivatedBlock.asm

3660	3D4YellowGemActivatedBlock.asm
3663	3DBTopRight.asm
3664	3D9TopLeft.asm
3667	3D5BlueGemActivatedBlock.asm
3668	3D7blue2.asm
3669	3D6red2.asm
366B	3D3RedGemActivatedBlock.asm

3673	3DCRight.asm
3674	3DALeft.asm

3683	3683TopRight.asm
3684	3684TopLeft.asm

3693	3693Right.asm
3694	3694Left.asm

37E0	37E0.asm
37E1	37E1.asm
37E2	37E2.asm

3890	3890.asm
3891	3891.asm
3892	3892.asm
3893	3893.asm

38C3	speedup.asm

3D50	3D50.asm
3D51	3D51.asm
3D52	3D52.asm
3D53	3D53.asm
3D54	3D54.asm
3D55	3D55.asm
3D56	3D56.asm
3D57	3D57.asm
3D58	3D58.asm
3D59	3D59.asm
3D5A	3D5A.asm
3D5B	3D5B.asm
3D5C	3D5C.asm
3D5D	3D5D.asm
3D5E	3D5E.asm
3d5F	3D5F.asm
