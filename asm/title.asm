header
lorom

!freeram = $60 		;dont edit this snn or everything will break

!exit_count = #$6A

org $809C67	;layer 3 menu frozen until set free by $60
autoclean JML asdf1

org $80941E
db $80,$01
db $00
db $80

org $809D6C
	autoclean jml new_exit_numbers
org $809DA6
	new_exit_numbers_jml_return:

org $809CAD	;enable title screen hdma
rep 3 : NOP

org $809C82	;stop title screen movement and looping
db $EA,$A9,$00


org $80941E	;disable circle fade in
	INC $0100
	LDA #$01
	STA $1DF5
rolf:
	JSL $7F8000
	LDA #$EC
	JMP $9440

org $809ABD	;see above
	JSR rolf


org $809EBA	;repoint arrow tilemap
db $A5,$30	
org $809EC1
db $A6,$30	

org $809AD0
NOP #3		;disable blinking


;/NEW LAYER 3 MENU

org $85B722
;     -       -       .       .       .       M       A       R       I       O               A               E       M       P       T       Y   
db $51,$EA,$00,$1F,$24,$38,$24,$38,$24,$38,$76,$31,$71,$31,$74,$31,$82,$30,$83,$30,$FC,$38,$71,$31,$FC,$38,$73,$31,$76,$31,$6F,$31,$2F,$31,$72,$31
db $52,$2A,$00,$1F,$24,$38,$24,$38,$24,$38,$76,$31,$71,$31,$74,$31,$82,$30,$83,$30,$FC,$38,$2C,$31,$FC,$38,$73,$31,$76,$31,$6F,$31,$2F,$31,$72,$31
db $52,$6A,$00,$1F,$24,$38,$24,$38,$24,$38,$76,$31,$71,$31,$74,$31,$82,$30,$83,$30,$FC,$38,$2D,$31,$FC,$38,$73,$31,$76,$31,$6F,$31,$2F,$31,$72,$31


org $80AE47
autoclean JML Boost0701

org $8084E2
dl OneTwoPlayerStim

;tweak this whenever the stripes are edited
org $809D55 : LDA #$8C
org $809DAB : SBC $AA
org $809D96 : LDY #$01

org $809D3A
autoclean JML stuff

org $809B7B
autoclean JML ultrastuff

freecode

stuff:
STZ $05
CPX #$CB
BEQ FileSelectCode
BRA EraseFileSelectCode

FileSelectCode:
PHB : PHK : PLB
STZ $0DDE
LDX #$00
REP #$10
LDY.w #$0000
-
LDA FileSelectStimStart,x
PHX : TYX
STA $7F837D,x
PLX : INX : INY
CPY.w #FileSelectStimEnd-FileSelectStimStart+$0001
BNE -
LDA #$32
STA $AA
PLB
JML $809D53

EraseFileSelectCode:
PHB : PHK : PLB
LDX #$00
REP #$10
LDY.w #$0000
-
LDA EraseFileSelectStimStart,x
PHX : TYX
STA $7F837D,x
PLX : INX : INY
CPY.w #EraseFileSelectStimEnd-EraseFileSelectStimStart+$0001
BNE -
SEP #$10
PLB
JML $809DB4


ultrastuff:
PHB : PHK : PLB
LDA $05
LDX #$00
LDA #$0A
STA $AA
REP #$10
LDY.w #$0000
-
LDA UltraStimStart,x
PHX : TYX
STA $7F837D,x
PLX : INX : INY
CPY.w #UltraStimEnd-UltraStimStart+$0001
BNE -
SEP #$10
LDA #$16
PLB
JML $809D57

FileSelectStimStart:
;incbin fileselect.stim
incbin "title/fileselect4.stim"
FileSelectStimEnd:

EraseFileSelectStimStart:
;incbin erasefileselect.stim
incbin "title/erasefileselect4.stim"
EraseFileSelectStimEnd:

UltraStimStart:
incbin "title/ultra.stim"
UltraStimEnd:

OneTwoPlayerStim:
;incbin 12player.stim
incbin "title/12player4.stim"

asdf1:
	LDA !freeram
	BEQ argh
	cmp.b #$02
	bcc +
	inc !freeram
	lda !freeram
	; Give LM's code time to clear out the button vram. 8 frames is barely noticeable by the user.
	cmp #$0A
	bcc argh
	bra .finish
	+
	; Enable Custom exanimation trigger 1 to show the press button to continue graphic
	lda $7FC0FC
	ora #$01
	sta $7FC0FC
	LDA $15
	ORA $17
	AND #$80
	BEQ argh
	inc !freeram

	; Disable Custom exanimation trigger 1 to hide the press button to continue graphic
	lda $7FC0FC
	and #$FE
	sta $7FC0FC
	bra argh

	.finish
	lda #$01
	sta !freeram
	LDA #$15
	STA $1DFC
	JML $809CA3

argh:
	JML $809C6C

new_exit_numbers:
	ldx $00
	cmp.b !exit_count
	bcc +
	lda.b #$16
	jsr draw_tile
	inx #2
	lda.b #$17
	jsr draw_tile
	inx #2
	lda.b #$18
	jsr draw_tile
	bra .return
	+
	pha
	lda.b #$FD
	jsr draw_tile
	inx #2
	jsr draw_tile
	inx #2
	jsr draw_tile
	pla
	jsr hex_to_dec
	cpy #$00
	bne +
		dex #2 ; Move the tile back one space if we have a 1 digit number here
	+
	jsr draw_tile
	dex #2
	tya
	beq .return
	jsr hex_to_dec
	jsr draw_tile
	dex #2
	tya
	beq .return
	jsr draw_tile
	.return
	jml new_exit_numbers_jml_return

draw_tile:
	pha
	clc
	adc #$22
	sta $7F837F,x
	lda.b #$39
	sta $7F8380,x
	pla
	rts

;Easier to copy this routine than deal with jslrts
hex_to_dec:
	ldy.b #$00
-
	cmp.b #$0A
	bcc +
	sbc.b #$0A
	iny	
	bra -
	+
	rts



;pasted from overworld exanimation
Boost0701:
;The original patch overflowed vblank, which generated some quite bad garbage. To fix this, I tried to optimize some other routines.
;Sadly, this was the only optimizable spot I found, and it wasn't enough. I had to split the uploads to two frames.
;But some extra VRAM time never hurts... (note: affects levels too)
LDA $0701
AND #$1F
ORA #$20
STA $2132

REP #$20
LDA $0701
LSR #2
SEP #$20;putting this SEP as soon as possible should save three more cycles...
LSR #3
AND #$1F
ORA #$40
STA $2132

LDA $0702
LSR #2
;I don't need AND #$1F here unless the top bit here is set, and it isn't.
ORA #$80
STA $2132
JML $80AE64