;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Block Bounce Unrestrictor patch
;
; should be /compatible/ with the other custom block bounce patch (work simultaneously with)
;  (but this one allows the blocks to configure the bounce sprites themselves)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
header
lorom
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!RAM_M16Tbl		= $7EC275	; same as the other patch, to avoid using more RAM than necessary
		!RAM_BounceTileTbl	= $7EC271

!addr = $0000
		
if read1($00ffd5) == $23
	sa1rom
	
	!addr			= $6000
	
	!RAM_M16Tbl		= $40C275		; you might want to change this to another BW-RAM address, if
	!RAM_BounceTileTbl	= $40C271		; necessary.
endif
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

org $02924E
		autoclean JML BlkBounceTileHack

org $02919F
		autoclean JML Map16TileHack

org $00BFB2
		autoclean JML GenerateTileHack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

freespace noram

BlkBounceTileHack:
	LDA.w $16C1|!addr,x
	CMP.b #$FF		; FF will generate with custom sprite tile (and map16 tile)
	BEQ .CustomSprTile
	CMP.b #$FD		; FD will generate with custom sprite tile
	BEQ .CustomSprTile
	LDA.w $1699|!addr,x
	TAX
	LDA.w $91F0,x		; DB = 02
	STA.w $0202|!addr,y
	JML $029258

.CustomSprTile
	LDA.l !RAM_BounceTileTbl,x
	STA.w $0202|!addr,y
	JML $029258

Map16TileHack:
	LDA.w $16C1|!addr,x
	CMP.b #$FE		; FE-FF will generate custom map16 tile (FF=custom sprite tile too)
	BCS .CustomM16Tile
	CMP.b #$0A
	JML $0291A4

.CustomM16Tile
	STA.b $9C
	PHX
	TXA
	ASL
	TAX
	REP #%00100000
	LDA.l !RAM_M16Tbl,x
	STA.w $0660|!addr
	SEP #%00100000
	PLX
	JML $0291BC

GenerateTileHack:
	STX.b $08	; \ hijacked
	STY.b $0A	; /
	SEP #%00110000
	LDA.b $9C
	CMP.b #$FE		; FE-FF will generate tile from $0660-$0661
	BCS .GenCustomTile
	JML $00BFB6

.GenCustomTile
	REP #%00110000
	LDA.b $98
	AND.w #$01F0
	STA.b $04
	LDA.b $9A
	LSR #4
	AND.w #$000F
	ORA.b $04
	TAY
	SEP #%00100000
	LDA.b [$6E],y
	AND.b #%11000000
	ORA.w $0661|!addr	;hi
	STA.b [$6E],y
	LDA.w $0660|!addr	;lo
	STA.b [$6B],y
	REP #%00100000
	LDA.w $0660|!addr
	ASL
	TAY
	PEA.w $BFB8
	JML $00C0FB
