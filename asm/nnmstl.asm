;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -NEW- NO MORE SPRITE TILE LIMITS ;)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; No More Sprite Tile Limits - SMWCP2 custom version
;; **REQUIRES STZ !RAM_SpriteTilesReservedDyn in SMWCP2's hijacks/level.asm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

lorom
header

!OAM_index = $18CC

; !SpriteTilesReservedEnabled	= 1
; !RAM_SpriteTilesReserved	= $18CA ; was 13E6
; !SpriteTilesReservedEnabledDyn	= 1
; !RAM_SpriteTilesReservedDyn	= $18CB ; was 13F2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

org $0180D2     
	autoclean JML PickOAMSlot

org $01B7BB
	autoclean JML finish_OAM_write_hijack


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

freecode

reset bytes

PickOAMSlot:
	LDA $1692			; \  if sprite header
	CMP #$10			;  | setting is not 10,
	BNE .default			; /  use the old code
	LDA $14C8,x			; \ it's not necessary to get an index
	BEQ .return			; / if this sprite doesn't exist
	LDA $9E,x			; \  give yoshi
	CMP #$35			;  | the first
	; BEQ .yoshi			; /  two tiles

	BNE SearchAlgorithm


.yoshi
	LDA #$28			; \ Yoshi always gets
	STA $15EA,x			; / first 2 tiles (28,2C)
.return
	JML $8180E5

.default
	PHX				; \
	TXA				;  | for when not using
	LDX $1692			;  | custom OAM pointer
	CLC				;  | routine, this is
	ADC $87F0B4,x			;  | the original SMW
	TAX				;  | code.
	LDA $87F000,x			;  |
	PLX				;  |
	STA $15EA,x			; /
	JML $8180E5
    
SearchAlgorithm:
	LDY !OAM_index
	CPY #$F0
	BCS .found_slot
	LDA #$F0
.OAM_loop
	CMP $0301,y			; we check the.. current slot???
	BNE .next
	CMP $0305,y			; and the next one
	BNE .next
	CMP $0309,y			; and the next one.......... (test)
	BNE .next
	CMP $030D,y			; and the next one..........
	BNE .next
	CMP $0311,y			; why
	BEQ .found_slot
	
.next
	INY
	INY
	INY
	INY
	CPY #$F0			; failsafe
	BCC .OAM_loop
	; BRA .found_slot
	
; .skip_2
	; TYA
	; CLC
	; ADC #$08
	; TAY
	; CPY #$F0
	; BCC .OAM_loop
	; BRA .found_slot

; .skip_3
	; TYA
	; CLC
	; ADC #$0C
	; TAY
	; CPY #$F0
	; BCC .OAM_loop
	; BRA .found_slot

; .skip_4
	; TYA
	; CLC
	; ADC #$10
	; TAY
	; CPY #$F0
	; BCC .OAM_loop

.found_slot
	TYA
	STA $15EA,x
.return
	JML $8180E5
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
finish_OAM_write_hijack:
	STY $0B
	STA $08
	INC
	ASL			; record the number of tiles that were drawn (+1)
	ASL
	ADC !OAM_index
	; ADC $15EA,x		; starting from the first that was actually used
	STA !OAM_index
	JML $81B7BF

print bytes