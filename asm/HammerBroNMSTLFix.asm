header
lorom

org $02DB98
autoclean	JML GetItTogetherHammerBro

freecode

GetItTogetherHammerBro:
	LDA $1692		; \  Sprite Header 10?
	CMP #$10		;  | If not, then skip
	BNE +			; /  the fix
	LDA $15EA,x		; \  In Sprite Header 10, we
	CLC : ADC #$10		;  | need to reassign the
	STA $15EA,y		; /  Hammer Bro's OAM index
+	PHX			; \
	TYX			;  | Restore Hijack
	PEA $DB9D-1		;  | (involves pseudo-JSR to HammerBroGfx)
	JML $82DAFD		; /
