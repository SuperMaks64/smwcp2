!kamekXPos = !actorVar2,x
!kamekYPos = !actorVar3,x
!kamekTimer = !actorVar4,x
!kamekState = !actorVar1,x	; 0 = normal
				; 1 = falling
				; 2 = run to Bowser
				; 3 = looking frantic

SetSpeakerToKamek:
	LDX !kamekIndex
	STX !currentSpeaker
	RTL

KamekSetToNormal:
	LDX !kamekIndex
	LDA #$00
	STA !kamekState
	RTL
	
KamekSetToFrantic:
	LDX !kamekIndex
	LDA #$03
	STA !kamekState
	RTL
	
KamekSquash:
	LDX !kamekIndex
	LDA #$D0
	STA $AA,x
	LDA #$F0
	STA $B6,x
	LDA #$01
	STA !kamekState
	RTL
	
KamekRunToBowser:
	LDX !kamekIndex
	JSL KamekINIT
	LDA #$00
	STA !kamekXPos
	STZ $AA,x
	STZ $B6,x
	LDX !kamekIndex
	LDA #$02
	STA !kamekState
	RTL


	
KamekCreate:
	REP #$20
	LDA.w #KamekINIT
	STA $00
	LDX.b #KamekINIT>>16
	STX $02
	
	LDA.w #KamekMAIN
	STA $03
	LDX.b #KamekMAIN>>16
	STX $05
	
	LDA.w #KamekNMI
	STA $06
	LDX.b #KamekNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !kamekIndex
	RTL

KamekINIT:
	LDA #$40
	STA !kamekYPos
	LDA #$73
	STA !kamekXPos
	RTL

KamekMAIN:
	
	PHK
	PLB

	LDA !kamekState
	CMP #$02
	BNE .notRunning
	LDA !kamekXPos
	CLC
	ADC #$03
	CMP #$71
	BCS .stopRunning
	STA !kamekXPos
	BRA .notRunning
.stopRunning
	LDA #$00
	STA !kamekState
	
.notRunning
	
	LDA !kamekState
	CMP #$01
	BNE .notFalling
	
	LDA !kamekXPos
	STA $E4,x
	LDA !kamekYPos
	STA $D8,x
	
	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position
	
	LDA $E4,x
	STA !kamekXPos
	LDA $D8,x
	STA !kamekYPos
.notFalling

	LDA !kamekYPos
	CMP #$80
	BCC +
	STZ $AA,x
	STZ $B6,x
	LDA #$80
	STA !kamekYPos
+
	
	JSR KamekDraw
	RTL

KamekNMI:
	RTL
	


KamekDraw:

	LDA !kamekState
	BEQ .normal
	CMP #$01
	BEQ .falling
	CMP #$02
	BEQ .running
	CMP #$03
	BEQ .frantic
	RTS
.running
	JMP .runningL
.frantic
	JMP .franticL
	
.normal
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$80 : STA $02
	LDA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : CLC : ADC #$10 : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$A0 : STA $02
	LDA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	RTS
	
.falling
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$A0 : STA $02
	LDA #$BC : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : CLC : ADC #$10 : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$80 : STA $02
	LDA #$BC : STA $03
	LDA #$02 : JSL DrawTile
	RTS
	
.runningL
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : STA $01
	LDA $13 : AND #$04 : LSR : CLC : ADC #$80 : STA $02
	LDA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : CLC : ADC #$10 : STA $01
	LDA $13 : AND #$04 : LSR : CLC : ADC #$90 : STA $02
	LDA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	RTS
	
.franticL
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$80 : STA $02
	LDA $13 : AND #$20 : ASL : ORA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA !kamekXPos : STA $00
	LDA !kamekYPos : CLC : ADC #$10 : STA $01
	LDA $13 : AND #$08 : LSR #3 : AND !textIsAppearing : ASL : CLC : ADC #$A0 : STA $02
	LDA $13 : AND #$20 : ASL : ORA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA !kamekXPos : CLC : ADC #$08 : STA $00
	LDA !kamekYPos : SEC : SBC #$08 : STA $01
	LDA $13 : AND #$04 : LSR : CLC : ADC #$84 : STA $02
	LDA #$3C : STA $03
	LDA #$02 : JSL DrawTile
	
	RTS


