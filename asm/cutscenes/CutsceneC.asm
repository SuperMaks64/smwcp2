!MonitorSpriteCHR = 	$0A24
!CutsceneCBackgroundCHR1 = $0119
!CutsceneCBackgroundCHR2 = $0110
!CutsceneCForegroundTilemap = $0A28
!CutsceneCBackgroundTilemap = $0A29


CutsceneCPalette:
incbin CutsceneC.rawpal
	
CSINITC:
	STZ $2121
	%StandardDMA(#$2122, CutsceneCPalette, #512, #$00) 
	
	REP #$30	
	LDA #!DocCrocGeneralExGFX		; |
	LDX #$C000>>1				; | Upload Doc Croc (SP1)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!MonitorSpriteCHR			; \ 
	LDX #$E000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	LDA #!CutsceneCBackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneCBackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneCForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneCBackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	LDA #$05
;	STA !backgroundColor
	LDA #$06
	STA !backgroundColor+1
	LDA #$07
	STA !backgroundColor+2
	
	REP #$20
	LDA #$00E8				; \ Layer 1 X
	STA $1A					; /
	LDA #$0000				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0000				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	
	LDA #$00
	STA !currentFont
	
	JSL MarioCreate
	JSL DocCrocMonitorCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$43 : JSL MarioSetTargetX

	JSL SetSpeakerToDocCrocMonitor
	
	RTL

	

CSCT1:
db $F4
%VWFASM(DocCrocMonitorLower)
db $F4
%VWFASM(MarioSetToVarWalkRight)
db $FF
db $FF
db "Hey, how's it going there, Marioni old pal? "
db "I see you've been taking quite a bit of interest in touring our facilities of late, as well as, "
%VWFASM(DocCrocMonitorSetArmsBehindHead)
db "ah, testing the durability of our equipment and personnel. ", !CCButton, !CCClearText
%VWFASM(DocCrocMonitorSetArmsNormal)
db "But nothing like a healthy sense of curiosity, I always say. "
db "The Boss-Man, too, has become aware of your hobby, and he thought that you'd be particularly interested in taking a tour of my personal labs. "
db "I've got all sorts of interesting projects going on down here which are sure to leave you screaming with delight, and quite possibly some other sensations as well. ", !CCButton, !CCClearText
db "Why don't you drop on by and take a look at some of them? ", !CCButton
db "Here, I'll give you a hand with that--"
db $FA
%VWFASM(DocCrocMonitorTrapdoor)
db $FF
db $F2
%VWFASM(MarioAddExclamationMark)
db $FF
db $F4
%VWFASM(MarioSetToFallThroughGround)
%VWFASM(MarioSetToGravityJumping)
%VWFASM(MarioSetPoseToHurt)
%VWFASMArg(MarioSetYSpeed, $F0)
db $FF
db $F8
%VWFASM(DocCrocMonitorRaise)
db $FF
db $FF
db $FF

db $FF, $E0

