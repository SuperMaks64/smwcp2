

!frankXPos = $E4,x
!frankYPos = $D8,x
!frankYSpeed = $AA,x
!frankXSpeed = $B6,x

!frankStaticX = $80			; The position Frank is set to. He doesn't move from this spot (except when he vibrates)


!frankState = !actorVar1,x		; 0 = falling, 1 = standing, 2 = crouching, 3 = roaring
!frankTimer = !actorVar2,x		; Used for the crouching state

FrankSetToRoaring:
	LDX !frankIndex
	LDA #$03
	STA !frankTimer
	LDA #$02
	STA !frankState
	RTL
	
	
FrankCreate:
	REP #$20
	LDA.w #FrankINIT
	STA $00
	LDX.b #FrankINIT>>16
	STX $02
	
	LDA.w #FrankMAIN
	STA $03
	LDX.b #FrankMAIN>>16
	STX $05
	
	LDA.w #FrankNMI
	STA $06
	LDX.b #FrankNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !frankIndex
	RTL

FrankINIT:
	STX !frankIndex
	LDA #$F0
	STA !frankYPos
	LDA #!frankStaticX
	STA !frankXPos
	RTL

FrankMAIN:
	STA $7FFFFF
	STZ $0D
	PHK
	PLB

	PHX				; Jump to Frank's current routine.
	LDA !frankState			;
	ASL
	TAX
	LDA FrankStateTable,x		;
	STA $00				;
	LDA FrankStateTable+1,x		;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	

	JSR FrankDraw
	RTL

FrankNMI:
	RTL
	

FrankStateTable:
dw FrankFalling, FrankStanding, FrankCrouching, FrankRoaring


FrankFalling:
	LDA !frankYPos
	BPL +
	LDA #$FF
	STA $14D4,x
+
	
	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position if it's falling.
	
	LDA $14D4,x
	BMI .noGroundCheck
	
	LDA !frankYPos
	CMP #$39
	BCC +
	
	LDA #$09
	STA $2143
	
	LDA #$10
	STA $1887
+
	
	LDA !frankYPos
	CMP #$3C
	BCC +
	LDA #$01
	STA !frankState
+
.noGroundCheck
	LDA #$08
	STA $0D

	RTS

FrankStanding:
	STZ $08
	
	LDA $13
	CLC
	ADC #$04
	AND #$10
	LSR
	LSR
	;STA $08
	
	;LDA #$00 : CLC : ADC $08 : STA $0D
	
	STA $0D
	
	RTS


FrankCrouching:
	LDA #$40
	STA $0D
	
	LDA !frankTimer
	DEC
	STA !frankTimer
	BNE +
	
	LDA !frankState
	INC
	STA !frankState
	
+

	RTS
	
	

FrankRoaring:
	LDA $13
	AND #$0F
	STZ $2140
	BNE +
	LDA #$25
	STA $2140
+
	STZ $08
	
	LDA $13
	AND #$04
	BNE +
	INC $08
+

	LDA #!frankStaticX
	CLC
	ADC #$06
	CLC
	ADC $08
	STA !frankXPos
	
	
	LDA #$0C
	STA $0D
	
	RTS











FrankDraw:
	LDA !frankXPos
	STA $00
	LDA !frankYPos
	STA $01
	LDA $13
	AND #$80
	ROL
	ROL
	ROL
	ORA #$3D
	STA $03
	
	; Left upper
	LDA !frankXPos : STA $00
	LDA !frankYPos : STA $01
	LDA $0D  : STA $02
	LDA #$02 : JSL DrawTile
	
	; Right upper
	LDA !frankXPos : CLC : ADC #$10 : STA $00
	; LDA $01 : STA $01
	LDA $0D : CLC : ADC #$02 : STA $02
	LDA #$02 : JSL DrawTile
	
	; Left lower
	LDA !frankXPos : STA $00
	LDA !frankYPos : CLC : ADC #$10 : STA $01
	LDA $0D : CLC : ADC #$20 : STA $02
	LDA #$02 : JSL DrawTile
	
	; Right lower
	LDA !frankXPos : CLC : ADC #$10 : STA $00
	; LDA $01 : STA $01
	LDA $0D : CLC : ADC #$22 : STA $02
	LDA #$02 : JSL DrawTile
	
	RTS
	
	

