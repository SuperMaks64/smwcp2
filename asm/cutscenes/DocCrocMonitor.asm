!docCrocMonitorX = !actorVar1,x
!docCrocMonitorY = !actorVar2,x
!docCrocMonitorDirection = !actorVar3,x		; Speed in either direction the monitor moves
!docCrocMonitorFace = !actorVar4,x		; 0 = normal, 1 = arms behind head
!docCrocMonitorTrapdoor = !actorVar5,x

SetSpeakerToDocCrocMonitor:
	LDX !docCrocMonitorIndex
	STX !currentSpeaker
	RTL


DocCrocMonitorCreate:
	REP #$20
	LDA.w #DocCrocMonitorINIT
	STA $00
	LDX.b #DocCrocMonitorINIT>>16
	STX $02
	
	LDA.w #DocCrocMonitorMAIN
	STA $03
	LDX.b #DocCrocMonitorMAIN>>16
	STX $05
	
	LDA.w #DocCrocMonitorNMI
	STA $06
	LDX.b #DocCrocMonitorNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !docCrocMonitorIndex
	RTL

DocCrocMonitorINIT:
	LDX !docCrocMonitorIndex
	LDA #$A0
	STA !docCrocMonitorX
	LDA #$F0
	STA !docCrocMonitorY
	RTL

DocCrocMonitorMAIN:
	LDA !docCrocMonitorY
	CLC : ADC !docCrocMonitorDirection
	CMP #$20
	BCC .changePosition
	CMP #$E0
	BCC .noChangePosition
.changePosition
	STA !docCrocMonitorY
.noChangePosition
	
	
	
	
	; Draw the monitor tiles
	LDA !docCrocMonitorX :                   STA $00
	LDA !docCrocMonitorY :                   STA $01
	LDA #$20 : STA $02
	LDA #$3D : STA $03
	LDA #$02 : JSL DrawTile
	
	
	LDA !docCrocMonitorX : CLC : ADC #$10 : STA $00
	LDA !docCrocMonitorY :                  STA $01
	LDA #$22 : STA $02
	LDA #$02 : JSL DrawTile
	
	
	LDA !docCrocMonitorX :                  STA $00
	LDA !docCrocMonitorY : CLC : ADC #$10 : STA $01
	LDA #$40 : STA $02
	LDA #$02 : JSL DrawTile
	
	
	LDA !docCrocMonitorX : CLC : ADC #$10 : STA $00
	LDA !docCrocMonitorY : CLC : ADC #$10 : STA $01
	LDA #$42 : STA $02
	LDA #$02 : JSL DrawTile
	
	
	LDA !docCrocMonitorX : CLC : ADC #$08 : STA $00
	LDA !docCrocMonitorY : CLC : ADC #$08 : STA $01
	
	; Draw the mouth tile
	LDA #$28
	STA $02
	LDA !textIsAppearing
	BEQ .noMouthAnimation
	
	LDA $13
	AND #$0C
	LSR
	CLC
	ADC #$28
	STA $02
.noMouthAnimation

	LDA #$3B
	STA $03
	LDA #$02 
	JSL DrawTile
	
	; Draw the head tile
	LDA !docCrocMonitorFace
	BEQ .drawNormalFace
	
	; Draw arms behind head here
	LDA $13
	AND #$10
	LSR #3
	CLC
	ADC #$44
	STA $02
	LDA #$02 
	JSL DrawTile
	
.drawNormalFace

	LDA #$24
	STA $02
	LDA #$02 
	JSL DrawTile
	
.doneWithHead
	
	; Draw the background on the monitor
	LDA !docCrocMonitorX : CLC : ADC #$08 : STA $00
	LDA !docCrocMonitorY : CLC : ADC #$08 : STA $01
	LDA #$08 : STA $02
	LDA #$3D : STA $03
	LDA #$02 : JSL DrawTile
	
	LDY #$06
-	
	TYA : ASL #4
	STA $0D
	
	LDA !docCrocMonitorX : CLC : ADC #$08 : STA $00
	LDA !docCrocMonitorY : SEC : SBC $0D : STA $01
	LDA #$0E : STA $02
	LDA #$3D : STA $03
	LDA #$02 : JSL DrawTile
	DEY
	BPL -	
	
	
	LDA !docCrocMonitorTrapdoor
	BNE +
	LDA #$30 : STA $00
	LDA #$5F : STA $01
	LDA #$60 : STA $02
	LDA #$37 : STA $03
	LDA #$02 : JSL DrawTile
	
	LDA #$40 : STA $00
	LDA #$5F : STA $01
	LDA #$02 : JSL DrawTile
	
	LDA #$50 : STA $00
	LDA #$5F : STA $01
	LDA #$02 : JSL DrawTile
	
	LDA #$60 : STA $00
	LDA #$5F : STA $01
	LDA #$02 : JSL DrawTile
	
	
	
	LDA #$30 : STA $00
	LDA #$6F : STA $01
	LDA #$60 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA #$40 : STA $00
	LDA #$6F : STA $01
	LDA #$02 : JSL DrawTile
	
	LDA #$50 : STA $00
	LDA #$6F : STA $01
	LDA #$02 : JSL DrawTile
	
	LDA #$60 : STA $00
	LDA #$6F : STA $01
	LDA #$02 : JSL DrawTile
+

	RTL


DocCrocMonitorNMI:
	RTL
	
DocCrocMonitorRaise:
	LDX !docCrocMonitorIndex
	LDA #$FF
	STA !docCrocMonitorDirection
	RTL

DocCrocMonitorLower:
	LDX !docCrocMonitorIndex
	LDA #$01
	STA !docCrocMonitorDirection
	RTL
	
DocCrocMonitorSetArmsBehindHead:
	LDX !docCrocMonitorIndex
	LDA #$01
	STA !docCrocMonitorFace
	RTL
	
DocCrocMonitorSetArmsNormal:
	LDX !docCrocMonitorIndex
	LDA #$00
	STA !docCrocMonitorFace
	RTL
	
DocCrocMonitorTrapdoor:
	LDX !docCrocMonitorIndex
	LDA #$01
	STA !docCrocMonitorTrapdoor
	RTL
	
	
DocCrocMonitorFaceTiles:
db $20, $22, $24, $26
db $2C, $2C, $2E, $2E
	
