!DuckCHR1 = 			$0A1D
!DuckCHR2 = 			$0A27
!CS8CarnivalBackTilemapExGFX = 	$0A1E
!CS8CarnivalForeTilemapExGFX = 	$0A1F
!CS8CarnivalForeChrExGFX = 	$0A21
!CS8CarnivalBackChrExGFX = 	$0A22

!CS8CarnivalForeTilemapExGFXSize = $1000
!CS8CarnivalBackTilemapExGFXSize = $1000

Cutscene8Palette:
incbin Cutscene8.rawpal
	
CSINIT8:
	STZ $2121
	%StandardDMA(#$2122, Cutscene8Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30
	LDA #!DuckCHR1				; \
	LDX #$C000>>1				; | Upload SP1
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!DuckCHR2				; \
	LDX #$D000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	REP #$30				; \
	LDA #!CS8CarnivalForeTilemapExGFX	; |
	LDX #!layer1MapAddr>>1			; | Upload foreground tilemap
	LDY #!CS8CarnivalForeTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /

	LDA #!CS8CarnivalBackTilemapExGFX	; \
	LDX #!layer2MapAddr>>1			; | Upload background tilemap
	LDY #!CS8CarnivalForeTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS8CarnivalForeChrExGFX		; \
	LDX #!layer1ChrAddr>>1			; | Upload foreground GFX
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS8CarnivalBackChrExGFX		; \
	LDX #!layer1ChrAddr+$1000>>1		; | Upload background GFX
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	LDA #$00
	STA !backgroundColor
	LDA #$00
	STA !backgroundColor+1
	LDA #$00
	STA !backgroundColor+2
	
	LDA #$3C
	STA $20
	STZ $21
	
	LDA #$D0
	STA $1C
	STZ $1D
	
	LDA #$D8
	STA $1A
	
	LDA #$00
	STA !currentFont
	
	JSL MarioCreate
	JSL BarrelCreate
	JSL BarrelCreate
	JSL BarkerDuckCreate
	JSL LuigiCreate
	JSL LuigiSetToFacingRight
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$43 : JSL MarioSetTargetX
	;JSL MarioSetWaitStateToNothing 
	;JSL MarioSetToVarWalkRight
	
	JSL SetSpeakerToBarkerDuck
	
	RTL

	

CS8T1:
db $F4
%VWFASM(MarioSetToVarWalkRight)
db $FF
db "Wasn't that fun, "
%VWFASM(BarkerArmsUp)
db "kid? ", $F1
%VWFASM(BarkerPointToMario)
db "But wait--", !CCZeroSpace, $F2, "I've got somethin' even "
%VWFASM(BarkerArmsUp)
db "better for you. I'll drop down two big ol' "
%VWFASM(BarkerPointToMario)
db "barrels here, one of 'em on top of this goomba. Find the goomba after I shuffle 'em 'round a bit "
%VWFASM(BarkerArmsUp)
db "and win a prize! "
db "What sort of prize? That's up to "
%VWFASM(BarkerPointToMario)
db "you! Put up a wager--", !CCZeroSpace, "find the goomba and win "
%VWFASM(BarkerSpinCane)
db "four times your wager! "
db "You have a fifty-fifty chance to win! ", !CCButton
%VWFASM(BarkerPointToMario)
;%VWFASM(BarkerArmsUp)
db "Whaddaya say, kid?", !CCButton, !CCClearText
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F8
%VWFASM(BarkerSetToNormal)
db "Oh, nothin' to wager?", $F8, !CCButton, !CCClearText



%VWFASM(SetSpeakerToLuigi)
db "Bro, wait!", !CCButton, !CCClearText
%VWFASM(LuigiSetToWalkingRight)
%VWFASMArg(LuigiSetX, $00)

db $F8
db !CCTextSpeed, $01
db "Whew, bro"
%VWFASM(LuigiSetToFacingLeft)
db ", I'm glad I finally caught"
%VWFASM(BarkerSetHeadFacingBack)
db " up wit"
%VWFASM(BarkerSetToNormal)
db "h you! I came running a"
%VWFASM(BarkerSetHeadFacingBack)
db "s soon as I found "
%VWFASM(BarkerSetToNormal)
db "that out you"
;%VWFASM(BarkerArmsUp)
%VWFASM(BarkerSpinCane)
%VWFASM(BarrelFirstFall)
db " forgot your--", !CCZeroSpace, !CCTextSpeed, $00, "Waaaa"
%VWFASM(MarioSetToAngry)
%VWFASM(BarkerSetToNormal)
;%VWFASM(BarkerStopSpinningCane)
db "augh!"
db $FF
;%VWFASM(BarkerArmsUp)
%VWFASM(BarkerSpinCane)
%VWFASM(BarrelSecondFall)
db $FF
db $F6
%VWFASM(BarkerSetToNormal)
;%VWFASM(BarkerSetToNormal)
db $FF, !CCButton, !CCClearText
%VWFASM(SetSpeakerToBarkerDuck)
db "Well "
%VWFASM(BarkerPointToMario)
db "hey, now, "
db "whaddya mean you have no wager, kid? "
%VWFASM(BarkerSkywardShout)
db "That fine green mustachioed fellow is perfect!", !CCButton, !CCClearText

%VWFASM(BarkerPointToMario)
db "Alrighty then! For a whopping four brand spankin' new of"

%VWFASM(BarkerSetToNormal)
%VWFASM(BarkerSetHeadFacingBack)
db $F6
db !CCTextSpeed, $01
db "_whoever that green guy was_"

db !CCTextSpeed, $00
db $F6
%VWFASM(BarkerPointToMario)
;%VWFASM(BarkerSetHeadFacingNormal)
db " all you need to do is pick the barrel with the goomba! "
%VWFASM(BarkerArmsUp)
db "Now_", !CCButton
%VWFASM(LuigiDelete)
%VWFASM(BarkerEraseGoomba)
%VWFASM(BarkerSpinCane)


%VWFASM(Barrel1SwapRight)
%VWFASM(Barrel2SwapLeft)
db $FF
db $FF
db $FF

%VWFASM(Barrel1SwapLeft)
%VWFASM(Barrel2SwapRight)
db $FF
db $FF
db $FF

%VWFASM(Barrel1SwapRight)
%VWFASM(Barrel2SwapLeft)
db $FF
db $FF
db $FF


%VWFASM(Barrel1SwapLeft)
%VWFASM(Barrel2SwapRight)
db $FF
db $FF

%VWFASM(BarkerPointToMario)
;%VWFASM(BarkerStopSpinningCane)
db "Wheeeeeere's the goomba?"
%VWFASM(BarkerDrawScaredLuigi)
db $F3
%VWFASM(BarkerSetToNormal)
db $F3
;%VWFASM(MarioSetPoseToStanding)
%VWFASM(MarioAddEllipsis)
db $FF
db $F8, !CCButton, !CCClearText
%VWFASM(MarioRemoveAboveHeadSymbol)
%VWFASM(MarioSetToGravityJumping)
%VWFASM(MarioSetWaitStateToNothing)
%VWFASMArg(MarioSetXSpeed, $1B)
%VWFASMArg(MarioSetYSpeed, $C0)
db $FF
db $F2
%VWFASM(Barrel2Hit)
%VWFASMArg(MarioSetXSpeed, $E5)
%VWFASMArg(MarioSetYSpeed, $D8)
db $FF
db $FF
%VWFASM(MarioSetPoseToHurt)
%VWFASM(MarioSetToGravityJumping)
%VWFASM(MarioSetWaitStateToFreeze)
%VWFASMArg(MarioSetXSpeed, $F8)
%VWFASMArg(MarioSetYSpeed, $F0)

db $FF
db "Ooooh, sorry kid, no goomba in there. ", !CCButton, "I'll just be taking "
%VWFASM(BarkerJumpToLuigi)
db "this, then."
db $F5
%VWFASM(BarkerRunOffScreen)
%VWFASM(MarioSetPoseToSad)
;db $F2
%VWFASM(BarkerDrawHangingLuigi)
db $FF, $FF, $FF, !CCButton, !CCClearText
%VWFASM(MarioSetPoseToStanding)
%VWFASM(MarioAddQuestionMark)
db $FF

%VWFASM(MarioRemoveAboveHeadSymbol)
%VWFASM(MarioSetToGravityJumping)
%VWFASM(MarioSetWaitStateToNothing)
%VWFASMArg(MarioSetXSpeed, $12)
%VWFASMArg(MarioSetYSpeed, $C0)
db $FF
db $F2
%VWFASM(Barrel1Hit)
%VWFASMArg(MarioSetXSpeed, $F5)
%VWFASMArg(MarioSetYSpeed, $D8)
db $FF, $FF
%VWFASM(MarioSetToAngry)
db $FF
%VWFASM(MarioRemoveAboveHeadSymbol)
%VWFASMArg(MarioSetTargetX, $F0)
%VWFASM(MarioSetToRunning)
%VWFASM(MarioSetWaitStateToFreeze)
%VWFASM(MarioSetToVarWalkRight)
db $FF, $FF
%VWFASM(CS1SetSceneToSlot2)

%VWFASM(BarkerStartScrolling)
%VWFASM(BarkerSetToRunningInPlace)
%VWFASMArg(BarkerSetXPos, $A0)
;%VWFASM(SetBarkerRunningWithLuigi)
db $FF
db "Hee hee hee, Norveg's gonna give me a MASSIVE bonus after pulling off a con like THAT one. Hindenbird's gonna be soooooo jealous!!"
db $F8, !CCButton, !CCClearText

%VWFASM(MarioSetWaitStateToVarMoveRight)
%VWFASM(MarioSetToRunning)
%VWFASM(MarioSetToVarWalkRight)
%VWFASMArg(MarioSetMovementSpeed, $01)
%VWFASMArg(MarioSetXPos, $00)
%VWFASMArg(MarioSetTargetX, $40)

db $FF, $FF, $F8
%VWFASMArg(MarioSetMovementSpeed, $00)
db $FF


%VWFASM(BarkerSetRunningHeadFacingBack)
db $FF
db $F5
%VWFASM(BarkerSetToRunningInPlace)
db $F2
;db $FF
db "Ah, jeez, this guy's faster than he looks. No chance of outrunning him while I'm lugging "
;%VWFASM(BarkerSetRunningHeadFacingBack)
db "green 'stache "
;%VWFASM(BarkerSetToRunningInPlace)
db "here around; better sit tight and call for backup.", !CCButton, !CCClearText
db "HEY, ", $F6
db "RUBE!!", !CCButton, !CCClearText
%VWFASM(BarkerStopScrolling)
%VWFASM(BarkerRunOffScreen)
%VWFASM(MarioSetToVarWalkRight)
%VWFASMArg(MarioSetTargetX, $F0)
%VWFASMArg(MarioSetMovementSpeed, $04)
%VWFASM(MarioSetWaitStateToFreeze)


db $FF, $FF, $FF, $E0

