!MadamMauAnimFrame = !actorVar1,x
!MadamMauOpenArms = !actorVar2,x

SetSpeakerToMadamMau2:
	LDX !madamMau2Index
	STX !currentSpeaker
	RTL
	
MadamMau2OpenArms:
	LDX !madamMau2Index
	LDA #$01
	STA !MadamMauOpenArms
	RTL

MadamMau2Create:
	REP #$20
	LDA.w #MadamMau2INIT
	STA $00
	LDX.b #MadamMau2INIT>>16
	STX $02
	
	LDA.w #MadamMau2MAIN
	STA $03
	LDX.b #MadamMau2MAIN>>16
	STX $05
	
	LDA.w #MadamMau2NMI
	STA $06
	LDX.b #MadamMau2NMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !madamMau2Index
RTL

MadamMau2Delete:
	LDX !madamMau2Index
	JSL DeleteActor
RTL

MadamMau2INIT:
	RTL
	
MadamMau2Frames:
db $00, $04, $08, $40, $44, $48, $4C, $40

MadamMau2MouthFrames:
db $00, $0E, $2C, $2E

MadamMau2MAIN:
	PHK
	PLB
	LDX !madamMau2Index
	PHX
	
	
	STA $7FFFFF
	LDA !MadamMauOpenArms
	BEQ +
	LDA $13
	AND #$03
	BNE +
	LDA !MadamMauAnimFrame
	CMP #$07
	BCS +
	INC
	STA !MadamMauAnimFrame
+
	
	
	;;; Draw the mouth
	
	LDA #$98		; \
	STA $00			; / X pos
	
	LDA #$37		; \ Y pos
	STA $01			; /
	
	LDA #$3C		; \
	STA $03			; / Property
	
	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	;LDA #$00		; Then draw nothing
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$0C		; |
	LSR			; | Otherwise, animate the mouth.
	LSR			; |
	TAX			; |
	BEQ +			; / (Don't draw tile 0)
	LDA MadamMau2MouthFrames,x
	
	STA $02
	LDA #$02
	JSL DrawTile
	
+
	PLX
	PHX
	LDA !MadamMauAnimFrame
	TAX
	
	;;; Draw the four tiles of the body
	LDA $00		:	SEC	:	SBC #$08	:	STA $00
	LDA $01		:	CLC	:	ADC #$08	:	STA $01
	LDA MadamMau2Frames,x					:	STA $02
	LDA #$02	:	JSL DrawTile
	
	LDA $00		:	CLC	:	ADC #$10	:	STA $00
;	LDA $01		:	CLC	:	ADC #$00	:	STA $01
	LDA MadamMau2Frames,x : CLC	:	ADC #$02	:	STA $02
	LDA #$02	:	JSL DrawTile
	
	LDA $00		:	SEC	:	SBC #$10	:	STA $00
	LDA $01		:	CLC	:	ADC #$10	:	STA $01
	LDA MadamMau2Frames,x : CLC	:	ADC #$20	:	STA $02
	LDA #$02	:	JSL DrawTile
	
	LDA $00		:	CLC	:	ADC #$10	:	STA $00
;	LDA $01		:	CLC	:	ADC #$10	:	STA $01
	LDA MadamMau2Frames,x : CLC	:	ADC #$22	:	STA $02
	LDA #$02	:	JSL DrawTile
	
	;;; Draw the pyramid hat
	
	LDA $00		:	SEC	:	SBC #$08	:	STA $00
	LDA $01		:	SEC	:	SBC #$20	:	STA $01
	LDA #$0C	:					:	STA $02
	LDA #$02	:	JSL DrawTile
	


	
	PLX
	RTL
	
MadamMau2NMI:
	RTL
