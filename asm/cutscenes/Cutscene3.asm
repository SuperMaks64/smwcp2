!MiscCutsceneGFX = $0A16
!TruntecForegroundTilemap = $0A17
!TruntecBackgroundTilemap = $0A18
!TruntecCHR1 = $01AB
!TruntecCHR2 = $01AA
!TruntecBackCHR = $0167

Cutscene3Palette:
incbin Cutscene3.rawpal
	
CSINIT3:
	STZ $2121
	%StandardDMA(#$2122, Cutscene3Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30				; \
	LDA #!TruntecForegroundTilemap		; |
	LDX #!layer1MapAddr>>1			; | Upload Truntec tilemap
	LDY #!CS1GardenTilemapExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	LDA #!TruntecCHR1			; \
	LDX #!layer1ChrAddr>>1			; | Upload Truntec GFX 1
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!TruntecCHR2			; \
	LDX #!layer1ChrAddr+$1000>>1		; | Upload Truntec GFX 2
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!TruntecBackCHR			; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 2
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	LDA #!MiscCutsceneGFX			; \
	LDX #$C000>>1				; | Upload SP1
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!TruntecBackgroundTilemap		; \
	LDX #!layer2MapAddr>>1			; | Upload the background
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #$0058
	STA $1C
	LDA #$FFE0
	STA $1A
	
	
	SEP #$30
	
	LDA #$04			; \
	STA !backgroundColor		; |
	LDA #$00			; |
	STA !backgroundColor+1		; |
	LDA #$00			; |
	STA !backgroundColor+2		; /
	
	LDA #$20
	STA $20
	STZ $21
	
	LDA #$01
	STA !currentFont
	
	JSL TruntecCreate
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$50 : JSL MarioSetTargetX
	JSL MarioSetWaitStateToNothing 
	JSL MarioSetToVarWalkRight
	
	RTL

;SetTrackingTo0:
;	STZ !trackingWidth
;	RTL
;
;SetTrackingTo1:
;	LDA #$01
;	STA !trackingWidth
;	RTL

;macro TruntecStaticStart()
;%VWFASM(SetTrackingTo0)
;db $EE, $EA, $03, $53
;endmacro

;macro TruntecStatic()
;db $54, $EE, $55, $EE
;endmacro

;macro TruntecEnd()
;db $54, $EE, $55, $56, $EA, $01, $EE
;%VWFASM(SetTrackingTo1)
;endmacro

CS3T1:
%VWFASMArg(MarioSetYPos, $48)
db $FF, $FF, $FF
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FB, $FF
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF, $FF
db $EA, $01
%VWFASM(SetSpeakerToTruntec)
db "AFFIRMATIVE.", $EC, "UNIT: ", $F5, "TRUNTEC", $52, $53, ".", $EC, "DUTIES INCLUDE CONVERSION OF LOCAL FAUNA AND FLORA AND CUSTOMER SERVICE." 
%VWFASM(SetSpeakerToNull)
db $EC
%VWFASM(SetSpeakerToTruntec)
db "PLEASE STATE THE NATURE OF YOUR BUSINESS."
%VWFASM(SetSpeakerToNull)
db $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
db $F3
%VWFASM(MarioSetToFrozen)
%VWFASM(SetSpeakerToTruntec)
db "IN ACCORDANCE WITH COMPANY REGULATIONS, PLEASE CONSENT TO THE FOLLOWING MANDATORY SCAN SO THAT WE MAY BETTER ASSESS YOUR QUERY. "
%VWFASM(SetSpeakerToNull)
%VWFASM(MarioResumeDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F3
%VWFASM(MarioAddQuestionMark)
db $FA, $EF
%VWFASM(MarioRemoveAboveHeadSymbol)
db $EC, $EB
%VWFASM(SetSpeakerToTruntec)
db "CONSENT ACKNOWLEDGED.", $EC, "SCANNING...", $EC
%VWFASM(TruntecSetToScannerMoving)
%VWFASM(SetSpeakerToNull)
db $FF, $F2
%VWFASM(TruntecSetToScanning)
%VWFASMArg(TruntecSetEyeState, $01)
db $EA, $00
db "...", $F8, $EC
db "...", $F8, $EC
db "...", $F8, $EC
db "...", $F8, $EC
db "...", $F8, $EC
db "...", $F8, $EC
db "...", $EF, "..............", $F8, $EC, $EB
%VWFASM(TruntecSetToScannerReturning)
%VWFASM(SetSpeakerToTruntec)
db $FF, $FF
%VWFASMArg(TruntecSetEyeState, $02)
db "WARNING! WARNING! DENIM, WORK BOOTS, AND LEVEL 5 MOUSTACHE DETECTED. SUBJECT DISPLAYS A 56.21694% PROBABILITY OF BEING AN UNAUTHORIZED LUMBERJACK. COMMENCING PREEMPTIVE EXTERMINATION."
%VWFASM(SetSpeakerToNull)
db $EF, $EB, $E0