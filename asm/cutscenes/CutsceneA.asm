!FrankCHR1 = 			$0A23

!CutsceneABackgroundCHR1 = $01BC
!CutsceneABackgroundCHR2 = $01BA
!CutsceneAForegroundTilemap = $0A31
!CutsceneABackgroundTilemap = $0A32

CutsceneAPalette:
incbin CutsceneA.rawpal
	
CSINITA:
	STZ $2121
	%StandardDMA(#$2122, CutsceneAPalette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	
	REP #$30	
	LDA #!DocCrocGeneralExGFX		; |
	LDX #$C000>>1				; | Upload Doc Croc (SP1)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!FrankCHR1				; \
	LDX #$E000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneABackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneABackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneAForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneABackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	LDA #$07
	STA !backgroundColor
	LDA #$13
	STA !backgroundColor+1
	LDA #$15
	STA !backgroundColor+2
	
	REP #$20
	LDA #$0000				; \ Layer 1 X
	STA $1A					; /
	LDA #$0070				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0060				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	LDA #$00
	STA !currentFont
	
	JSL MarioCreate
	JSL DocCrocCreate
	LDA #$A8 : JSL DocCrocSetXPos
	LDA #$55 : JSL DocCrocSetYPos
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$43 : JSL MarioSetTargetX

	JSL SetSpeakerToDocCroc
	JSL DocCrocSetToFacingAwayFromCamera
	
	RTL

	

CSAT1:

db $FF, $FF
%VWFASM(MarioSetToVarWalkRight)
db $FF
db $FF
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F8
%VWFASM(DocCrocSetToStanding)
db $F8

%VWFASM(DocCrocSetToStandingArmsOut)
db "Hey there, Mario; "
%VWFASM(DocCrocSetToPointing)
db "long time no see. "
%VWFASM(DocCrocSetToStanding)
db "This place is "
%VWFASM(DocCrocSetToStandingArmsOut)
db "fascinating, don't you think? "
db "The forces at play here allow these two climatological extremes to exist side-", !CCZeroSpace, "by-", !CCZeroSpace, "side, "
db "without neutralizing each other--", !CCZeroSpace, "astounding, no? "
db "Imagine what we could do if we could understand and redirect these forces--", !CCZeroSpace
%VWFASM(DocCrocSetToStanding)
db $FF, "oh, but you probably guessed that's exactly what this facility here is all about.", !CCButton, !CCClearText

db "We're making pretty good progress, if I may say so myself. "
%VWFASM(DocCrocSetToStandingArmsOut)
db "Why, we've even tried putting these principles to work in a living being!", !CCButton, !CCClearText

%VWFASM(FrankCreate)
db $FF
db $FF
%VWFASM(DocCrocSetToPointing)
db "I'd like you to meet Frank. "
%VWFASM(DocCrocSetToStandingArmsOut)
db "He's a true child of this place--", !CCZeroSpace, "fiery temper, heart of ice, all that lovely sort of stuff you'd expect, really. ", !CCButton, !CCClearText
%VWFASM(DocCrocSetToStanding)
db "Now, me, I've got to be heading off to make a report to our Glorious Leader, "
db "but why don't "
%VWFASM(DocCrocSetToPointing)
db "you two stick around and chat a bit? "
%VWFASM(DocCrocSetToStandingArmsOut)
db "I'm sure you'll have a crackling discussion; a real friction-free relationship. "
db "The two of you will get along like fire and ice, like caesium and water, like Guelphs and Ghibellines. ", !CCButton, !CCClearText
%VWFASM(DocCrocSetToHat)
db "Well, I don't want to come off as cold, but it's time for me to beat the heat. ", !CCButton, !CCClearText
%VWFASM(DocCrocSetToFlying)
db "Arrivederci!", !CCButton, !CCClearText
db $FF
db $FF
db $FF
%VWFASM(MarioSetToFacingLeft)
db $FF
db $FF
%VWFASM(MarioSetToFacingRight)
db $FF
db $FF
%VWFASM(MarioSetToFacingLeft)
db $FF
%VWFASM(MarioSetToFacingRight)
db $FF
db $FF
db $F8
%VWFASM(MarioAddEllipsis)
db $FF
db $FF
db $FF
db $FF, !CCButton
%VWFASM(MarioRemoveAboveHeadSymbol)
%VWFASM(MarioSetToGravityJumping)
%VWFASMArg(MarioSetYSpeed, $C0)
db $F7
%VWFASM(FrankSetToRoaring)
db $F3
%VWFASM(MarioSendFlyingOffScreen)


db $FF, $FF, $FF, $FF, $E0

