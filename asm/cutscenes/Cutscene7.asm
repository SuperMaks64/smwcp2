!MauCHR1 = $0A1C

Cutscene7Palette:
incbin Cutscene1.rawpal
	
CSINIT7:
	STZ $2121
	%StandardDMA(#$2122, Cutscene7Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30
	LDA #!MauCHR1				; \
	LDX #$C000>>1				; | Upload SP1
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	REP #$30				; \
	LDA #!CS1DesertTilemapExGFX		; |
	LDX #!layer1MapAddr>>1			; | Upload foreground tilemap
	LDY #!CS1GardenTilemapExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	LDA #!CS1DesertBackTilemapExGFX		; \
	LDX #!layer2MapAddr>>1			; | Upload background tilemap
	LDY #!CS1DesertBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	

	LDA #!CS1GardenChrExGFX			; \ (Also includes the desert tiles)
	LDX #!layer1ChrAddr>>1			; | Upload foreground GFX
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1BackgroundChrExGFX		; \
	LDX #!layer1ChrAddr+$1000>>1		; | Upload background GFX
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #$00C0
	STA $1C
	
	SEP #$30
	
	LDA #$1F
	STA !backgroundColor
	LDA #$1B
	STA !backgroundColor+1
	LDA #$18
	STA !backgroundColor+2
	
	LDA #$90
	STA $20
	STZ $21
	
	LDA #$4A
	STA $1A
	
	LDA #$00
	STA !currentFont
	
	JSL MadamMau2Create
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$58 : JSL MarioSetTargetX
	;JSL MarioSetWaitStateToNothing 
	;JSL MarioSetToVarWalkRight
	
	JSL SetSpeakerToMadamMau2
	
	RTL


CS7T1:
db $EA, $01
db $F4
%VWFASM(MarioSetToVarWalkRight)
db $FF
db "So, you have arrived. I've been observing you. "
db "Such a lively figure: a brightly clad, capering clown, dancing his merry jig among the bleached and withered bones of a long-", $EE, "dead civilization. "
db "An amusing contrast. "
db "Yet you do not seem amused. "
db "Such seriousness, such ardor--", $EE, $F4, "which makes the scene all the more comedic still. "
db "Yet I still wonder: what little cause of yours could possibly drive you so?", $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF, $F8
db "Norveg is <poisoning the world>? "
db "A fittingly melodramatic response from our sober-", $EE, "faced Scaramouche. "
db "But he himself is dependent on the world--", $EE, $F4, "and he will succumb to the poison long before the planet does. ", $EF, $EB
db "The arrogance of you short-", $EE, "lived beings, to imagine any of your actions can so easily and permanently destroy the world that sustains you. "
db "The earth will not go down quite so easily, you will find. ", $EF, $EB
db "It can be changed, and it will change in ways that will prove most inhospitable to YOU--", $EE, $F4
db "the people here learned that when they mismanaged their delicate resources, and the desert swept in over their proud cities--", $EE, $F4
db "but the world itself will still endure, in one form or another. ", $EF, $EB
db "Do not confuse your own fragile existence with that of the planet.", $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db "Why do I assist him? "
db "Why should I not? "
db "The long-", $EE, "term result is the same, no matter what any of us may do. ", $EF, $EB
db "Sooner or later,", $EC
db "Norveg will perish,", $F2, $EC
db "you will perish,", $F2, $EC
db "I will perish,", $F2, $EC
db "every fish in the ocean,", $F2, $EC
db "every cactus in the desert,", $F2, $EC
db "every tree and blade of grass in the Oasis.", $EF, $EB
db "And some day, the world itself will perish--", $EE, "not by any mortal hand, but splendidly immolated by the sun. "
db "We cannot avoid this. "
db "The best we may hope for is to determine some of the meanderings of our path towards oblivion. "
db "And if Norveg seeks to perform a particularly remarkable dive onto the refuse heap of history--", $EE, $F4, "well, I would not begrudge him that.", $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF, $F8
db "Ah? "
db "I take it you seek a similar terminus? "
db "A dramatic exeunt for our comedic player? "
db "Very well. "
db "Come! "
%VWFASM(MadamMau2OpenArms)
db "I will give you the brief glimmer of glory you so desperately seek!", $EF, $E0

