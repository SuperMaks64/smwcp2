!NorvegSpriteCHR = 	$060D

!CutsceneFBackgroundCHR1 = $0203
!CutsceneFBackgroundCHR2 = $060A
!CutsceneFBackgroundCHR3 = $060B
!CutsceneFForegroundTilemap = $0A37
!CutsceneFBackgroundTilemap = $0A38

CutsceneFPalette:
incbin CutsceneF.rawpal
	
CSINITF:
	STZ $2121
	%StandardDMA(#$2122, CutsceneFPalette, #512, #$00) 
	
	REP #$30	
	LDA #!NorvegSpriteCHR			; |
	LDX #$D000>>1				; | Upload Norveg
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	;lda #$060c
	;ldx #$7000
	;ldy #$1000
	;jsl $39f459

	lda #!CutsceneFBackgroundCHR1
	ldx #$A000>>1
	ldy #!CS1GardenChrExGFXSize
	jsl DecompressAndUploadGFXFile

	lda #!CutsceneFBackgroundCHR2
	ldx #$B000>>1
	ldy #!CS1GardenChrExGFXSize
	jsl DecompressAndUploadGFXFile

	lda #!CutsceneFBackgroundCHR3
	ldx #$C000>>1
	ldy #!CS1GardenChrExGFXSize
	jsl DecompressAndUploadGFXFile

	lda #!CutsceneFForegroundTilemap
	ldx #$2800
	ldy #!CS1SeaBackTilemapExGFXSize
	JSL DecompressAndUploadGFXFile

	lda #!CutsceneFBackgroundTilemap
	ldx #$3000
	ldy #!CS1SeaBackTilemapExGFXSize
	jsl DecompressAndUploadGFXFile
	
	
	SEP #$30

	lda #$1f
	sta !backgroundColor
	lda #$1f
	sta !backgroundColor+1
	lda #$1f
	sta !backgroundColor+2
	lda #$01
	sta !enableColorMath
	lda #$00
	sta $44
	lda #$92
	sta $40
	rep #$20

	lda #$0010
	sta $1a
	sta $1e
	lda #$0060
	sta $1c
	sta $20
	sep #$20
	lda #$00
	sta !currentFont
	
	JSL MarioCreate
	JSL NorvegCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$53 : JSL MarioSetTargetX

	;LDA #$0F
	;STA !norvegYPos
	;LDA #$6F
	;STA !norvegXPos

	LDA #$8F : JSL NorvegSetXPos
	LDA #$30 : JSL NorvegSetYPos

	
	RTL

	

CSFT1:
db $F4
%VWFASM(MarioSetToVarWalkRight)

%VWFASM(SetSpeakerToNorveg)

db $FF
db $FF
db "So, you made it here after all. I've got to admit, you do have some skill; I can see why Doc was so impressed with you. ", $EF, $EB
db "Pity we got off on the wrong foot; you could have made a fine chief of security for us here at Norveg Industries. In hindsight, I think we can safely say contracting that out to third parties did not prove to be in the company's best interest.Ah, but live and learn, live and learn_", $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F8
db "<Join me, Mario>? Ha-", $EE, "ha, don't worry, I wouldn't try anything that trite. "
db "Maybe in another life, another universe, but here and now, it seems we have a pretty clear conflict of interests, you and I. "
db "Can't run your business according to idle dreams, after all--", $EE, "we're still in the research and development stage of -that- particular project.", $EF, $EB
db "Like I said, it's a simple cost/", $EE, "benefit analysis, and I'm afraid that you, my friend, have proven yourself decidedly bad for business. ", $EF, $EB
db "But don't get too distraught--", $EE, "you'll still get to make a contribution to our fine enterprise by playing a crucial role in the testing of our prototype Super Masher Weaponized for Corporate Protection. "
%VWFASM(MarioAddQuestionMark)
%VWFASM(NorvegSetToRobotReveal)
db "So rest assured that you'll have a radiant spot in our company's history nonetheless.", $EF
db $FF, $E0

