
!bowserChairX = $80
!bowserChairY = $40




BowserChairCreate:
	REP #$20
	LDA.w #BowserChairINIT
	STA $00
	LDX.b #BowserChairINIT>>16
	STX $02
	
	LDA.w #BowserChairMAIN
	STA $03
	LDX.b #BowserChairMAIN>>16
	STX $05
	
	LDA.w #BowserChairNMI
	STA $06
	LDX.b #BowserChairNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !bowserChairIndex
	RTL

BowserChairINIT:
	RTL

BowserChairMAIN:

	; (2, 0)
	LDA #!bowserChairX : CLC : ADC #$20 : STA $00
	LDA #!bowserChairY :		      STA $01
	LDA #$04 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile

	; (3, 0)
	LDA #!bowserChairX : CLC : ADC #$30 : STA $00
	LDA #!bowserChairY :		      STA $01
	LDA #$06 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile

	; (0, 1)
	LDA #!bowserChairX :                  STA $00
	LDA #!bowserChairY : CLC : ADC #$10 : STA $01
	LDA #$20 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile

	; (1, 1)
	LDA #!bowserChairX : CLC : ADC #$10 : STA $00
	LDA #!bowserChairY : CLC : ADC #$10 : STA $01
	LDA #$22 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile

	; (2, 1)
	LDA #!bowserChairX : CLC : ADC #$20 : STA $00
	LDA #!bowserChairY : CLC : ADC #$10 : STA $01
	LDA #$24 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile

	; (3, 1)
	LDA #!bowserChairX : CLC : ADC #$30 : STA $00
	LDA #!bowserChairY : CLC : ADC #$10 : STA $01
	LDA #$26 : STA $02
	LDA #$3E : STA $03
	LDA #$02 : JSL DrawTile
	RTL


BowserChairNMI:
	RTL
	
