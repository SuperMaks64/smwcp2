!DocCrocGeneralExGFX = $0A13

!Cutscene2BackgroundCHR1 = $0248
!Cutscene2BackgroundCHR2 = $011A
!Cutscene2ForegroundTilemap = $0A2A
!Cutscene2BackgroundTilemap = $0A2B

	
Cutscene2Palette:
incbin Cutscene2.rawpal

CSINIT2:
	STZ $2121
	%StandardDMA(#$2122, Cutscene2Palette, #512, #$00) 
	
	REP #$30				; \
	LDA #!DocCrocGeneralExGFX		; |
	LDX #$C000>>1				; | Upload Doc Croc (SP1)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene2BackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene2BackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene2ForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene2BackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	JSL CS1SetBlueBackgroundColor
	
	REP #$20
	LDA #$00D8				; \ Layer 1 X
	STA $1A					; /
	LDA #$0060				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0000				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	LDA #$20
	STA $20
	STZ $21
	
	JSL DocCrocCreate
	JSL MarioCreate
	JSL DocCrocSetToFacingAwayFromCamera
	JSL SetSpeakerToDocCroc
	LDA #$98 : JSL DocCrocSetXPos
	LDA #$00 : JSL MarioSetXPos
	LDA #$60 : JSL MarioSetTargetX
	JSL MarioSetWaitStateToNothing 
	JSL MarioSetToVarWalkRight
	
	RTL
	


CS2T1:
db $FF, $FF, $FF
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F8
%VWFASM(DocCrocSetToStanding)
db $F8
db "Ah! Hey there, son, and welcome to the Toad Highlands branch of Norveg Industries! ", $EF, "How can I--"
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $EB
db $FF, $FF
%VWFASM(DocCrocSetToStandingArmsOut)
db "Ah, yes, I am indeed! The one and only Alexandre Felix Gustav Remmington Maxwell Sergio Jean-Claude Friedrich Petronius Catallus Temujin Archipelago Krokialaukis, doctor of Doctor of Robotics, Bioengineering and 'Pataphysics, at your service. "
%VWFASM(DocCrocSetToPointing)
db "What can I do you for, son?", $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
%VWFASM(DocCrocSetToStanding)
%VWFASM(SetSpeakerToNull)
db ".", $F3, " .", $F3, " .", $F3, " . "
%VWFASM(DocCrocSetArmsBehindHead)
%VWFASM(SetSpeakerToDocCroc)
db "Ah, ", $F7, "you ", $F9, $EA, $02, "want to" 
%VWFASM(DocCrocSetArmsNormal)
db " talk about", $EA, $01
db " our ", $EA,$01, "company's recent expansion into ", $EA,$00, "the Mushroom Kingdom and surrounding territories? ", $F5, "Well. ", $EF, $EB
db "That's a very interesting topic, and I'd love to stay and discuss it with you--", $EE, "but I'm afraid duty calls, "
%VWFASM(DocCrocSetToStandingArmsOut)
db "very important meeting with the boss man, dontchaknow. "
%VWFASM(DocCrocSetToPointing)
db "But tell you what, "
%VWFASM(DocCrocSetToStandingArmsOut)
db "why not take the matter up with Truntec*, our customer service representative? I'm sure he'll give you just the help you need.", $EF, $EB
%VWFASM(DocCrocSetToStanding)
db "Well, I've gotta fly--"
%VWFASM(DocCrocSetToHat)
db $FF
db "and I do mean that quite literally.", $EC, $EC, $EF, "Up, up, and aw"
%VWFASM(DocCrocSetToFlying)
db "aaaaay!"
db $FF, $F8
db $EF
%VWFASM(MarioSetToWalkingRight)
db $FF, $FF, $FF, $E0

CS2T2:
CS2T3:
CS2T4:
CS2T5:
CS2T6: