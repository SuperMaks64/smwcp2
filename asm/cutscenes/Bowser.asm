;!bowserState = !actorVar1,x	;
!bowserXPos = !actorVar2,x	;
!bowserYPos = !actorVar3,x	;
!bowserTimer = !actorVar4,x	;
!bowserAnimation = !actorVar5,x	; 00000001 = animate arms
				; 00000010 = eyes wide
				; 00000100 = relaxing (overrides the other two)
				; 00001000 = repeated jump (not actual animation, just a normal flag)
				; 00010000 = squash Kamek on land (again, just a flag)
				; 00100000 = shake ground on landing (flag)
				; 01000000 = set to relax on landing (flag)
				
!bowserGroundPosition = $30

SetSpeakerToBowser:
	LDX !bowserIndex
	STX !currentSpeaker
	RTL
	
BowserSetToNormalArms:
	LDX !bowserIndex
	LDA !bowserAnimation
	AND #%11111110
	STA !bowserAnimation
	RTL


BowserSetToWavingArms:
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%00000001
	STA !bowserAnimation
	RTL
	
BowserSetToNormalEyes:
	LDX !bowserIndex
	LDA !bowserAnimation
	AND #%11111101
	STA !bowserAnimation
	RTL
	
BowserSetToWideEyes:
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%00000010
	STA !bowserAnimation
	RTL

BowserSetToNormal:
	LDX !bowserIndex
	LDA !bowserAnimation
	AND #%11111011
	STA !bowserAnimation
	RTL
	
BowserSetToRelaxing:
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%00000100
	STA !bowserAnimation
	STZ $AA,x
	RTL

BowserSetToRepeatedJumping:
	JSL BowserJump
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%00001000
	STA !bowserAnimation
	RTL
	
BowserStopJumping:
	LDX !bowserIndex
	LDA !bowserAnimation
	AND #%11110111
	STA !bowserAnimation
	RTL
	
BowserSetToSquashKamek:
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%00010000
	STA !bowserAnimation
	RTL
	
BowserJump:
	LDX !bowserIndex
	LDA #$D0
	STA $AA,x
	LDA !bowserAnimation
	ORA #%00100000
	STA !bowserAnimation
	RTL
	
BowserSetToRelaxOnLanding:
	JSL BowserJump
	LDX !bowserIndex
	LDA !bowserAnimation
	ORA #%01000000
	STA !bowserAnimation
	RTL

	

	
BowserCreate:
	REP #$20
	LDA.w #BowserINIT
	STA $00
	LDX.b #BowserINIT>>16
	STX $02
	
	LDA.w #BowserMAIN
	STA $03
	LDX.b #BowserMAIN>>16
	STX $05
	
	LDA.w #BowserNMI
	STA $06
	LDX.b #BowserNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !bowserIndex
	RTL

BowserINIT:
	LDA #!bowserGroundPosition
	STA !bowserYPos
	LDA #$80
	STA !bowserXPos
	JSL BowserSetToRelaxing
	RTL

BowserMAIN:
	
	PHK
	PLB
	
;	LDA $AA,x
;	PHA

	LDA !bowserXPos
	STA $E4,x
	LDA !bowserYPos
	STA $D8,x
	
	
	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position
	
;	PLA				; Restore gravity, otherwise it's too fast (for some reason?)
;	BMI +
;	CMP #$20
;	BCS .skip
;+
;	INC				; And just modify it manually.
;.skip
;	STA $AA,x
	
	LDA $D8,x
	CMP #!bowserGroundPosition
	BCS .tooFarDown

	LDA $D8,x
	STA !bowserYPos
	INC $AA,x
	BRA .groundStuffFinished
.tooFarDown

	; We're about to try to move Bowser into the ground. We don't want him there. Don't put him there.
	LDA #!bowserGroundPosition
	STA !bowserYPos
	
	;STZ $AA,x
	LDA !bowserAnimation
	AND #%00100000
	BEQ .groundStuffFinished
	
	
	LDA !bowserAnimation
	AND #%01000000			; Check for relax on land
	BEQ +
	JSL BowserSetToRelaxing
	BRA .groundStuffFinished
+
	
	LDA !bowserAnimation
	AND #%11011111
	STA !bowserAnimation
	
	LDA #$10
	STA $1887
	
	LDA #$09
	STA $2143

	LDA #!bowserGroundPosition
	STA !bowserYPos
	
	LDA !bowserAnimation
	AND #%00001000			; Check for repeated jumping
	BEQ +
	
	JSL BowserJump
+

	LDA !bowserAnimation		; Check for squashing Kamek
	AND #%00010000
	BEQ .groundStuffFinished
	JSL KamekSquash
	
	LDX !bowserIndex
	LDA !bowserAnimation
	AND #%11101111
	STA !bowserAnimation
	
.groundStuffFinished

	LDA $E4,x
	STA !bowserXPos
	
	
	JSR BowserDraw
	
	RTL

BowserNMI:
	RTL
	


BowserTiles:
; 0 - Relaxing,						mouth closed
db $4A, $4C, $4E
db $6A, $6C, $6E
db $00, $02, $4A

; 1 - Standing, 	arms up, 	eyes normal, 	mouth closed
db $4A, $08, $0A
db $4A, $28, $2A
db $4A, $4A, $4A

; 2 - Standing, 	arms down, 	eyes normal, 	mouth closed
db $4A, $0C, $0A
db $4A, $2C, $2E
db $4A, $4A, $4A

; 3 - Standing, 	arms up, 	eyes wide, 	mouth closed
db $4A, $08, $0A
db $4A, $28, $2A
db $4A, $4A, $4A

; 4 - Standing, 	arms down, 	eyes wide, 	mouth closed
db $4A, $44, $0A
db $4A, $0E, $2A
db $4A, $4A, $4A

; 5 - Relaxing,						mouth open
db $4A, $46, $48
db $6A, $66, $68
db $00, $02, $4A

; 6 - Standing, 	arms up, 	eyes normal, 	mouth open
db $4A, $42, $0A
db $4A, $62, $2A
db $4A, $4A, $4A

; 7 - Standing, 	arms down, 	eyes normal, 	mouth open
db $4A, $42, $0A
db $4A, $0E, $2E
db $4A, $4A, $4A

; 8 - Standing, 	arms up, 	eyes wide, 	mouth open
db $4A, $44, $0A
db $4A, $62, $2A
db $4A, $4A, $4A

; 9 - Standing, 	arms down, 	eyes wide, 	mouth open
db $4A, $44, $0A
db $4A, $0E, $2A
db $4A, $4A, $4A

BowserTilesXPositions:
db $00, $10, $20
db $00, $10, $20
db $00, $10, $20

BowserTilesYPositions:
db $00, $00, $00
db $10, $10, $10
db $18, $18, $18


BowserDraw:

	STZ $0A					; X offset - used when Bowser's not relaxing
	STZ $0B					; Y offset - used when Bowser's not relaxing
	
	STZ $0C					; $0C is the current frame
						; When Bowser is standing, 1 is added.
						; When Bowser's arms are up, 1 is added.
						; When Bowser's eyes are wide, 2 is added.
						; When Bowser's mouth is open, 5 is added.
	
	LDA !bowserAnimation			; First, check if we're relaxing, and if so, skip the eyes and arms stuff.  We will do mouth stuff, though.
	AND #$04
	BNE .doneWithArmsAndEyes
	
	LDA #$F7
	STA $0A
	LDA #$05
	STA $0B
	
.standing					
	INC $0C					; When Bowser is standing, 1 is added.

	LDA !bowserAnimation
	AND #$01
	BEQ .notWavingArms
	
	LDA $13					; Animate the arms based on $13
	AND #$10
	BEQ .notWavingArms
	INC $0C					; When Bowser's arms are up, 1 is added.
	
	
	
.notWavingArms
	LDA !bowserAnimation
	AND #$02
	BEQ .notWideEyes
	INC $0C					; When Bowser's eyes are wide, 2 is added.
	INC $0C					;
	
	
.notWideEyes
	
	
	
.doneWithArmsAndEyes	
	
	LDA !textIsAppearing
	BEQ +
	LDA $13
	AND #$08
	BEQ +
	
	LDA $0C					; \ 
	CLC					; | When Bowser's mouth is open, 5 is added.
	ADC #$05				; |
	STA $0C					; /
+

	LDA $0C					; \ 
	INC					; | (Increment, since we count backwards when drawing tiles)
	STA $4202				; |
	LDA #$09				; |
	STA $4203				; | Multiply by 9 (the number of tiles in each frame)
	NOP #4					; |
	LDY $4216				; /
	
	DEY
	

	LDA !bowserXPos
	STA $0D
	LDA !bowserYPos
	STA $0C
	

PHX

	LDX #$08
.drawLoop	
	
	LDA BowserTiles,y
	CMP #$4A
	BEQ .skip
	STA $02

	LDA BowserTilesXPositions,x
	CLC
	ADC $0D
	CLC
	ADC $0A
	STA $00
	
	LDA BowserTilesYPositions,x
	CLC
	ADC $0C
	CLC
	ADC $0B
	STA $01
	
	
	LDA #$3E
	STA $03
	
	LDA #$02
	JSL DrawTile

.skip	
	DEY
	DEX
	BPL .drawLoop
	
PLX
	RTS

	


