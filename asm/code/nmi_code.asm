nmi_code:
	REP #$20
	LDA !level
	CMP #$0170
	BEQ DoconveyorNMI
	CMP #$0172
	BEQ DoconveyorNMI

	SEP #$20
	lda.l $7E1908
	cmp #$04
	bne +
	jsr WheelCode
	lda.l $7E1908
	+
	cmp #$05
	bne +
	jsr norveg_code
	+
	.return
	RTS
!ConveyorLeftTileLocation = #$0180
!ConveyorRightTileLocation = #$0190
DoconveyorNMI:
	sep #$20
	lda $0100
	cmp.b #$10
	bcc nmi_code_return
	PHB
	PHX
	
	STZ $0A					; \ $0A-$0B is a counter that lets us know which graphics we're uploading.
	STZ $0B					; / 0 = upload "left" conveyor, 1 = upload "right" conveyor.
	
	LDA #$01
	STA $4300				; DMA Control

	LDA #$18				; Write to VRAM
	STA $4301

	JSR GetCurrentSongPositionIn44TimeNMI
	STA $4204
	STZ $4205
	LDA #$06
	STA $4206
	NOP #8

	LDA $4214
	STA $4202				; \ 
	LDA #$20				; | Multiply the frame times $20 (size of a tile in bytes)
	STA $4203				; /
	NOP #4
	LDA $4216				; \
	STA $4202				; | Now multiply by 12.
	LDA #12					; | This is how far into the file we need to go to get our graphics.
	STA $4203				; /
	nop #8
	rep #$20
	lda $4216
	sta $0E

	lda !UploadedConveyorLastFrame
	beq +
		jmp OneWayConveyorNMILoop
	+

ConveyorNMILoop:
	JSR GetCurrentSongMeasureIn44TimeNMI	; \ 
	REP #$20				; |
	sta $08					; |
	AND #$0001				; |
	EOR $0A					; | ($0B = upload the "other" graphics, used to save space in a loop).
	BEQ +					; | Every other measure we "flip" the conveyor graphics.
	LDA #$0C00				; | Which just means loading the graphics 0xC00 bytes later in the file.
+						; |
	STA $0C					; /
						
	
	
	PHK					; 
	PLB					; 
	
	REP #$20				; \ 
	LDA.w #ConveyorGraphics			; |
	CLC					; |
	ADC $0C					; |
	CLC					; | Add that to the location of the graphics
	ADC $0E				; | And save that to the DMA registers
	STA $4302				; /
	SEP #$20				;
	PHB					; \ 
	PLA					; | Do the bank byte as well.
	STA $4304				; /
	
	LDX $0A					; \ 
	LDA #$00				; | 
	STA $2116				; | Set the VRAM address depending on where in the loop we are
	LDA ConveyorTileLocation,x		; |
	STA $2117				; /

	REP #$20				
	LDA #$0180				; Size (constant)
	STA $4305				
	SEP #$20				



	LDA #$01				; Start the transfer
	STA $420B
	INC $0A
	LDA $0A
	CMP #$02
	BEQ +
	JMP ConveyorNMILoop	
	
+
	PLX
	PLB
	inc !UploadedConveyorLastFrame
	RTS

OneWayConveyorNMILoop:
	rep #$20
	lda $0A
	BEQ +					; | Every other measure we "flip" the conveyor graphics.
	LDA #$0C00				; | Which just means loading the graphics 0xC00 bytes later in the file.
+						; |
	STA $0C					; /
	
	PHK					; 
	PLB					; 
	
	REP #$20				; \ 
	LDA.w #ConveyorGraphics			; |
	CLC					; |
	ADC $0C					; |
	CLC					; | Add that to the location of the graphics
	ADC $0E				; | And save that to the DMA registers
	STA $4302				; /
	SEP #$20				;
	PHB					; \ 
	PLA					; | Do the bank byte as well.
	STA $4304				; /
	
	LDX $0A					; \ 
	LDA #$00				; | 
	STA $2116				; | Set the VRAM address depending on where in the loop we are
	LDA OneWayConveyorTileLocation,x		; |
	STA $2117				; /

	REP #$20				
	LDA #$0180				; Size (constant)
	STA $4305				
	SEP #$20				



	LDA #$01				; Start the transfer
	STA $420B
	INC $0A
	LDA $0A
	CMP #$02
	BEQ +
	JMP OneWayConveyorNMILoop	
	
+
	PLX
	PLB
	stz !UploadedConveyorLastFrame
	RTS
	
WheelCode:
	!StateTimer = $0AF5
	 PHX
	 PHY
	 LDA #$07
	 STA $2105
	 STZ $210B
	 STZ $2107

	 lda $13D4
	 bne .dont_animate_water
	 ; Animate the water
	 lda $13
	 lsr #3
	 and #$03
	 tax
	 rep #$20
	 lda #$9D00
	 -
	 cpx #$00
	 beq +
	 clc
	 adc #$0200
	 dex
	 bra -
	 +
	 sta $4342
	 ldx #$7E
	 stx $4344

	 lda #$5060
	 sta $2116

	 lda #$1801
	 sta $4340
	 lda #$0100
	 sta $4345

	 ldx #$10		;Free DMA channel
	 stx $420B		;Enable DMA

	 sep #$20

	 .dont_animate_water

	 LDA !StateTimer
	 CMP #$01
	 BEQ +
	 JMP .endnmi
	 +
	 LDA #$10
	 SEC
	 SBC $140B ;!BlockCounter in sprite
	 STA $00
	 STZ $01
	 REP #$20
	 LDA #$5420
	 SEC
	 SBC $00
	 SBC $00
	 STA $02
	 STA $2116		;VRAM Address
	 LDA #$180D		;43x0 - 2 registers write once
	 STA $4340		;43x1 - $2118 - VRAM Data Write lo byte
	 LDA.w #.BlankTile	;Mode 7 Graphics Offset (Inserted with xkas)
	 STA $4342		;DMA Source Address Offset
	 LDX.b #.BlankTile>>16	; Mode 7 Graphics Bank
	 STX $4344		;DMA Source Address Bank
	 LDA #$0004
	 STA $4345
	 STA $04
		
	 LDX #$10		;Free DMA channel
	 STX $420B		;Enable DMA
	 
	 LDA $02
	 CLC
	 ADC #$0020
	 STA $2116
	 LDA #$0004
	 STA $4345
	 
	 LDX #$10		;Free DMA channel
	 STX $420B		;Enable DMA
	 
	 LDA #$5800
	 CLC
	 ADC $00
	 ADC $00
	 STA $02
	 STA $2116		;VRAM Address
	 LDA #$180D		;43x0 - 2 registers write once
	 STA $4340		;43x1 - $2118 - VRAM Data Write lo byte
	 LDA.w #.BlankTile	;Mode 7 Graphics Offset (Inserted with xkas)
	 STA $4342		;DMA Source Address Offset
	 LDX.b #.BlankTile>>16	; Mode 7 Graphics Bank
	 STX $4344		;DMA Source Address Bank
	 LDA #$0004
	 STA $4345
	 STA $04
		
	 LDX #$10		;Free DMA channel
	 STX $420B		;Enable DMA
	 
	 LDA $02
	 CLC
	 ADC #$0020
	 STA $2116
	 LDA #$0004
	 STA $4345
	 
	 LDX #$10		;Free DMA channel
	 STX $420B		;Enable DMA
	 
	 SEP #$20

	 .endnmi
	 PLY
	 PLX
	 RTS
	 
	 .BlankTile
		db $00 	;Repeated byte uploaded, it's a blank tile so it doesn't matter what the pallette and flip are

!robot_state = $1B7F
norveg_code:
	phb
	phk
	plb
	lda !robot_state
	cmp #$04
	beq +
		lda.b #$20
		sta $40
		stz $0701
		stz $0702
		bra  .return
	+

	lda #%00000000
	sta $44

	lda #%00111111
	sta $2131
	sta $40

	lda $14
	and #$0F
	asl
	tax
	rep #$20
	lda .colors,x
	sta $0701
	sep #$20

	.return
	plb
	rts

	.colors
		dw $0000
		dw $0004
		dw $0008
		dw $000C
		dw $0010
		dw $0014
		dw $0018
		dw $001C
		dw $001F
		dw $001F
		dw $001C
		dw $0018
		dw $0014
		dw $0010
		dw $000C
		dw $0008
		dw $0004
		dw $0000
	 
	

	
GetCurrentSongPositionIn44TimeNMI:
	REP #$20
	LDA $7FB004
	STA $4204
	SEP #$20
	LDA #$C0
	STA $4206
	NOP #8
	LDA $4216
	RTS
	
	
GetCurrentSongMeasureIn44TimeNMI:
	REP #$20
	LDA $7FB004
	STA $4204
	SEP #$20
	LDA #$C0
	STA $4206
	NOP #8
	LDA $4214
	RTS

ConveyorTileLocation:
db $2E, $2D
OneWayConveyorTileLocation:
db $18, $19
	
ConveyorGraphics:
	incbin conveyor.bin